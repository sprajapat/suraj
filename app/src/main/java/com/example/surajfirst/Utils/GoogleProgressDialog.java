package com.example.surajfirst.Utils;

import android.app.ProgressDialog;
import android.content.Context;


/**
 * Created by admin on 9/6/2017.
 */

public class GoogleProgressDialog {


    ///FOR RUN THIS DIAOLG BOX SUCCESSFULLY YOU HAVE TO COMPILE THIS DEPENDENCY IN GRADLE.BUILD FILE  compile 'com.github.castorflex.smoothprogressbar:library-circular:1.1.0'
    Context context;
    ProgressDialog dialog;



    public GoogleProgressDialog(Context context) {
        this.context = context;
    }

    public void show() {
        try {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Loading....");
           /* dialog = new ProgressDialog(context, R.style.full_screen_dialog) {
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.google_progress_dialoge);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

                   *//* Glide.with(context)
                            .load(R.drawable.loder_image)
                            .into(loader);*//*
                }

            };*/
            dialog.show();
            dialog.setCancelable(false);
        } catch (Exception e) {
            dismiss();
        }
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

/*

    ///FOR RUN THIS DIAOLG BOX SUCCESSFULLY YOU HAVE TO COMPILE THIS DEPENDENCY IN GRADLE.BUILD FILE  compile 'com.github.castorflex.smoothprogressbar:library-circular:1.1.0'
    Context context;
    ProgressDialog dialog;
    private CircularProgressBar mCircularProgressBar;
    private Interpolator mCurrentInterpolator;
    private int mStrokeWidth = 4;
    private float mSpeed = 1f;


    public GoogleProgressDialog(Context context) {
        this.context = context;
    }

    public void ShowDialoge() {
        try {
            dialog = new ProgressDialog(context, R.style.full_screen_dialog) {
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.full_dialoge);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
                    mCircularProgressBar = (CircularProgressBar) findViewById(R.id.progressbar_circular);
                    updateValues();
                }

            };
            dialog.show();
            dialog.setCancelable(false);
        } catch (Exception e) {
            dismiss();
        }
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void updateValues() {
        CircularProgressDrawable circularProgressDrawable;
        CircularProgressDrawable.Builder b = new CircularProgressDrawable.Builder(context)
                .colors(context.getResources().getIntArray(R.array.gplus_colors))
                .sweepSpeed(mSpeed)
                .rotationSpeed(mSpeed)
                .strokeWidth(dpToPx(mStrokeWidth));
        if (mCurrentInterpolator != null) {
            b.sweepInterpolator(mCurrentInterpolator);
        }
        mCircularProgressBar.setIndeterminateDrawable(circularProgressDrawable = b.build());

        // /!\ Terrible hack, do not do this at homeO!
        circularProgressDrawable.setBounds(0,
                0,
                mCircularProgressBar.getWidth(),
                mCircularProgressBar.getHeight());
        mCircularProgressBar.setVisibility(View.INVISIBLE);
        mCircularProgressBar.setVisibility(View.VISIBLE);
    }

    private int dpToPx(int dp) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, r.getDisplayMetrics());
        return px;
    }

*/

}
