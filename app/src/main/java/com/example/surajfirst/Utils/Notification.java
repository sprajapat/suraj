package com.example.surajfirst.Utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.example.surajfirst.MainActivity;
import com.example.surajfirst.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Notification {
    public static void DefaultNotification(Context context, String title, String text, String ticker) {
        String strtitle = title;
        String strtext = text;

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.locationo)
                .setTicker(ticker)
                .setContentTitle(title)
                .setContentText(text)
                .addAction(R.mipmap.ic_launcher, "Action Button", pIntent)
                .setContentIntent(pIntent)
                .setAutoCancel(true);

        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());
    }

    public static void CustomNotification(Context context, String packageName, int layout, String title, String text, String ticker) {

        RemoteViews remoteViews = new RemoteViews(packageName, layout);
        String strtitle = title;
        String strtext = text;

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.locationo)
                .setTicker(ticker)
                .setAutoCancel(true)
                .setContentIntent(pIntent)
                .setContent(remoteViews);

        remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);
        remoteViews.setImageViewResource(R.id.imagenotiright, R.drawable.img_logo);
        remoteViews.setTextViewText(R.id.title, title);
        remoteViews.setTextViewText(R.id.text, text);

        android.app.NotificationManager notificationmanager = (android.app.NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.notify(0, builder.build());

    }
}
