package com.example.surajfirst.Utils;

public class CommonResBean extends SuperCastClass {
    /**
     * status : success
     * message : messages.user_logout
     * data : null
     */

    private String status;
    private String message;
    private Object data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }





}
