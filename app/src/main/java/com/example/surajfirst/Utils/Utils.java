package com.example.surajfirst.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by admin on 5/28/2018.
 */

public class Utils {

    public static final String TABLE_NAME = "ContactDB";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_NUMBER = "number";


    public static void createTables(JsonObject jsonObject) throws JSONException {
    }

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_NUMBER + " TEXT"
                    + ")";

    public static String getUserId(Context context) {
        return nullSafe(getPref(context, ApiConstants.loginUserId, ""), "");
    }

    public static String getStringForGetMethod(Map<String, String> Mapvalue) {
        try {

            StringBuilder finalValue = new StringBuilder();
            for (Map.Entry<String, String> entry : Mapvalue.entrySet()) {
                finalValue.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
            return finalValue.substring(0, finalValue.length() - 1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getDeviceId(Context context) {
        return "123";
    }

    public static void setPref(Context context, String pref, String val) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putString(pref, val);
        e.commit();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getCurrentTimeStemp() {
        try {
            Long tsLong = System.currentTimeMillis() / 1000;
            return tsLong.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String encodeSha56(String data) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(ApiConstants.API_SECRET_KEY_VALUE.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            String endCode = Base64.encodeToString(sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8)), Base64.DEFAULT);

            return endCode.replace("\n", "").replace("\r", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getUserToken(Context context) {
        String str = "Bearer " + nullSafe(getPref(context, ApiConstants.loginUserToken, ""), "");
        return str;
    }

    public static String getPref(Context c, String pref, String val) {
        return PreferenceManager.getDefaultSharedPreferences(c).getString(pref,
                val);
    }

    public static String nullSafe(String strValue, String defaultValue) {
        return strValue == null || strValue.trim().length() <= 0 || strValue.equalsIgnoreCase("null") ? defaultValue : strValue;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public void setStarColor(RatingBar ratingBar) {
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#FDDD4A"), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.parseColor("#c7c7c7"), PorterDuff.Mode.SRC_ATOP);
    }

}
