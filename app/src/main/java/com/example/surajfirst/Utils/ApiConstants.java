package com.example.surajfirst.Utils;

import java.util.Date;

/**
 * Created by admin on 12/18/2017.
 */

public class ApiConstants {
    /*--------------- suraj -----------*/
    public static final String ROOT_URL = "http://ostaad.stage02.obdemo.com/api/v1/";
    public static final String ROOT_URL2 = "http://ostaad.stage02.obdemo.com/";
    //            "https://demonuts.com/Demonuts/JsonTest/Tennis/simplelogin.php";
//            "https://api.androidhive.info/volley/string_response.html";
//            "http://192.168.101.1/Android/Api.php?apicall=";

    public static final String verify_verification_code = ROOT_URL + "verify-verification-code";

    public static final String android = "android";
    public static final String URL_SIGNUP = ROOT_URL + "signup";
    public static final String URL_VERIFY = ROOT_URL + "verify-verification-code";
    public static final String URL_LOGIN = ROOT_URL + "login";
    public static final String POST = "POST";
    public static final String Authorization = "Authorization";
    public static final String loginUserToken = "loginUserToken";

    public static final String ENCODE_BASE_URL = "api/v1/";
    public static final String singup = ENCODE_BASE_URL + "signup";
    public static final String login = ENCODE_BASE_URL + "login";
    public static final String loginUserId = "loginUserId";
    public static final String BASE_URL = " http://ostaad.stage02.obdemo.com/api/v1/";
    //---------------parameters-----------------------------------------------------
    public static final String DEVICE_TYPE = "device_type";
    public static final String DEVICE_ID = "device_id";
    public static final String API_KEY = "OS-API-KEY";
    public static final String API_SECRET = "OS-API-SIGN";
    public static final String API_TIMESTAMP = "OS-API-TIMESTAMP";

    public static final String API_KEY_VALUE = "f96355923cf08aac54656e10977dd2b2";
    public static String API_TIMESTAMP_VALUE = "";
    public static String API_SECRET_VALUE = "";


    public static final String API_SECRET_KEY_VALUE = "943d51473ebdec9b171bc95c07e268556850ee12";

    public static final String SENDER_ID = "sender_id";
    public static final String RECEIVER_ID = "receiver_id";
    public static final String LANG_CODE = "lang_code";
    /*public static final String PLAN_ID = "plan_id";
    public static final String LOCATION = "location";
    public static final String MESSAGE = "message";*/

    public static final String TITLE = "title";
    public static final String CATEGORY_ID = "category_id";
    /* public static final String LOCATION = "location";*/
    public static final String TOWN = "town";
    public static final String CITY = "city";
    public static final String COMPANY_WEBSITE = "company_website";
    public static final String OPENING_TIME_FROM = "opening_time_from";
    public static final String OPENING_TIME_TO = "opening_time_to";
    public static final String ADDRESS = "address";
    public static final String TYPE_OF_AD = "type_of_ad";
    public static final String BRAND = "brand";
    public static final String KILOMERTER_FROM = "kilometer_from";
    public static final String KILOMETER_TO = "kilometer_to";
    public static final String MODEL = "model";
    public static final String ADD_CURRENCY = "add_currency";
    public static final String ADD_TYPE = "add_type";
    public static final String ADD_NAME = "add_name";
    public static final String ADD_EMAIL = "add_email";
    public static final String DISHES = "dishes";
    public static final String TOWN_DISTRICT = "town_district";
    public static final String CATEGORY = "category";
    public static final String SUBCATEGORIES = "subcategories";
    public static final String JOB_SECTOR = "job_sector";
    public static final String CONTRACT_TYPE = "contract_type";
    public static final String METER = "meter";
    public static final String NO_OF_ROOMS = "no_of_rooms";
    public static final String RENT_PER_MONTH = "rent_per_month";
    public static final String KILOMETER = "kilometer";
    public static final String TRANSMISSION = "transmission";
    public static final String VEHICLE_YEAR = "vehicle_year";
    public static final String VEHICLE_TYPE = "vehicle_type";
    public static final String SEATING = "seating";
    public static final String COLOR = "color";
    public static final String DOORS = "doors";
    public static final String DETAILED_DESCRIPTION = "detailed_description";
    public static final String ADD_PRICE = "add_price";
    public static final String SALARY = "salary";
    public static final String SALARY_PERIOD = "salary_period";
    public static final String SIGNUP_WITH = "signup_with";
    public static final String SOCIAL_ID = "social_id";
    public static final String FUEL = "fuel";
    public static final String YEAR_FROM = "year_from";
    public static final String YEAR_TO = "year_to";
    public static final String PRICE = "price";
    public static final String AD_TYPE = "ad_type";
    public static final String SORT_BY = "sortBy";
    public static final String ORDER = "order";
    public static final String PAGE = "page";

    //--------signup data & Login Data & verification ----------------
    public static final String NAME = "name";
    public static final String EMAIl = "email";
    public static final String PASSWORD = "password";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String CONFIRM_PASSWORD = "confirm_password";
    public static final String USER_ROLE_ID = " user_role_id";
    public static final String USER_ID = "user_id";
    public static final String USER_ID_DRIVERS = "3";
    public static final String USER_ID_CUSTOMERS = "2";
    public static final String verification_code = "verification_code";


  /*  public static final String LASTNAME = "last_name";
    public static final String REPEAT_PASSWORD = "repeat_password";
    public static final String PHONE = "phone_number";
    public static final String OFFERS_CHECK = "receive_offers_check";
    public static final String NAME = "name";
    public static final String USERNAME = "username";
    public static final String TERMS = "accept_terms_conditions";
    public static final String AUTHORIZATION = "Authorization";
    public static final String Bearer = "Bearer";
    public static final String SLUG = "slug";

    public static final String RECEIVE_OFFERS_CHECK = "1";
    public static final String TERMS_CONDITIONS = "1";*/


    //----------------------------values-------------------------------------------------
    public static final String ANDROID = "Android";


    //--------------------------------URLs-----------------------------------------------
    // public static final String LOGIN_URL = "login";
    public static final String GET_HOME_PAGE_SLOGEN = "get-homeO-page-slogen";
    public static final String GET_CATEGORIES_SUBCATEGORIES = "get-categories-subcategories";
    public static final String CITIES_URL = "get-cities";
    public static final String GET_TOWN_URL = "get-town";
    public static final String ALL_ADS = "all-ads";
    public static final String GET_BRAND = "get-brand";
    public static final String GET_KILOMETERS = "get-kilometer-range";
    public static final String GET_YEAR_RANG = "get-year-range";
    public static final String GET_MODEL = "get-model";
    public static final String GET_TRANSMISSION = "get-transmission";
    public static final String GET_FUEL = "get-fuel";

    public static final String LOGIN_URL = "login";
    public static final String SIGNUP_URL = "singup";
    public static final String GET_FILTERS = "get-filters";
    public static final String LOGOUT_URL = "logout";
    public static final String JOBDETAIL_URL = "job-detail";
    public static final String ADDETAIL_URL = "ad-detail";
    public static final String HOUSING_DETAIL_URL = "hosing-detail";
    public static final String MYFAVADS_URL = "favorites";
    public static final String MYCREDIT_URL = "my-credits";
    public static final String INBOX_MESSAGE_URL = "inbox";
    public static final String KEN_CREDITS_URL = "ken-credits";
    public static final String SEND_MESSAGE_URL = "send-message";
    public static final String MY_ADS_URL = "my-ads";
    public static final String BOOSTER_URL = "booster";
    public static final String GET_MESSAGE_URL = "get-message";
    public static final String LAST_MESSAGE_URL = "last-message";
    public static final String PROFILE_URL = "profile";
    public static final String POSTADD_URL = "post-ads";
    public static final String CREDIT_PAYMENT_URL = "ads-payment-with-credit";

    //-------------------------------Others----------------------------------------------
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static String ALL_SELECTED_USER_NOTICE;
    public static String ALL_SELECTED_LANGUAGE_NAME;
    public static String SelectUserCleraTag;

    public static Date DateSelected;
    public static int timeFlag;


    public static String SUCCESS = "success";
    public static String DEVICE_TYPE_ANDROID = "android";
    public static String SELECTED_LANGUAGE = "en";
    public static String ANDROID_DEVICE_ID = "test";
    public static String Code = "code";
    public static String Category = "";

    //------------------------------Api Tag----------------------
    public static String LOGIN_COACH_TAG = "Coach";


}
