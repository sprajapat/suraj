package com.example.surajfirst.Utils;

public class CommanConstants {
    public static final String fontBold = "Poppins-Bold.ttf";
    public static final String fontRegular = "Poppins-Regular.ttf";

    public static final String emailPattern = "([a-zA-Z0-9._-]+@[a-z]+\\.([com]{3})|([in]{2}))|([0-9]{10})";

    //    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.[com]{3}";
    public static final String phonePattern = "[0-9]";
    public static final String POST_Method = "POST";
    public static final String GET_Method = "GET";
    public static final String DROP_TXT = "DROP_TXT";
    public static final String PICKUP_TXT = "PICKUP_TXT";
    /*for ride details*/
    public static final String PERSON = "PERSON";
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    public static final String STATUS = "STATUS";
    public static final String PAYTYPE = "PAYTYPE";
    public static final String AMOUNT = "AMOUNT";
    public static final String IMAGEID = "IMAGEID";
    public static final String PAYMODE = "PAYMODE";

}
