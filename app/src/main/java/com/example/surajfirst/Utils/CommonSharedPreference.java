package com.example.surajfirst.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.surajfirst.Beans.LoginResBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by admin on 12/19/2017.
 */

public class CommonSharedPreference {

    private static String PREF_KEY = "preference";
    private static String storebalance;
    static SharedPreferences.Editor editor;

    //.............................................................................................

   /* public String getFCMToken(Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String mToken = sharedPref.getString("mToken",null);
        if (mToken==null){
            mToken = FirebaseInstanceId.getInstance().getToken();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("mToken",mToken);
            editor.apply();
        }
        return mToken;
    }


    public void clearNotificationData(Context context , String mKeyValue){
        SharedPreferences sharedPreferences=context.getSharedPreferences(ApiConstants.NotificationPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(mKeyValue,null);
        editor.apply();
    }
    public void setNotificationData(Context context , NotificationBeanListClass notificationBean, String mKeyValue) {
        Log.e("", "setNotificationData:>>>> "+mKeyValue );
//        Log.e("", "setNotificationData:>>>>notificationBean size "+notificationBean.getNotificationBeanList().size() );
        SharedPreferences sharedPreferences=context.getSharedPreferences(ApiConstants.NotificationPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(notificationBean);
        editor.putString(mKeyValue, json);
        editor.apply();
    }
    public NotificationBeanListClass getAllPendingNotificationList(Context context, String mKeyValue ) {
        Log.e("", "getAllPendingNotificationList:>>>>> "+mKeyValue );
        SharedPreferences pref = context.getSharedPreferences(ApiConstants.NotificationPref, Context.MODE_PRIVATE);
        String json = pref.getString(mKeyValue, null);
        Gson gson = new Gson();
        Type type = new TypeToken<NotificationBeanListClass>() {
        }.getType();

        NotificationBeanListClass beans = null;
        try {
            beans = gson.fromJson(json, type);
        } catch (Exception e) {
            return null;
        }
        // Log.e("+", "getAllPendingNotificationList: >>>>>Size"+beans.getNotificationBeanList().size() );
        return beans;
    }*/

    public LoginResBean getLoginBeanPref(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString("Login_PREF", null);
        Gson gson = new Gson();
        Type type = new TypeToken<LoginResBean>() {
        }.getType();

        LoginResBean beans = null;
        try {
            beans = gson.fromJson(json, type);
        } catch (Exception e) {
            return null;
        }
        return beans;
    }


    public void setLoginBeanPref(Context context, LoginResBean beans) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString("Login_PREF", json);
        editor.apply();
    }


    public String getLangBeanPref(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString("Lang_PREF", null);
        Gson gson = new Gson();
        Type type = new TypeToken<String>() {
        }.getType();

        String beans = null;
        try {
            beans = gson.fromJson(json, type);
        } catch (Exception e) {
            return null;
        }
        return beans;
    }

    public void setLangBeanPref(Context context, String beans) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString("Lang_PREF", json);
        editor.apply();
    }


    public String getAppFirstPref(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString("App_First_PREF", null);
        Gson gson = new Gson();
        Type type = new TypeToken<String>() {
        }.getType();

        String beans = null;
        try {
            beans = gson.fromJson(json, type);
        } catch (Exception e) {
            return null;
        }
        return beans;
    }

    public void setAppFirstPref(Context context, String beans) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(beans);
        editor.putString("App_First_PREF", json);
        editor.apply();
    }


    //.............................................................................................


    public void Logout_User(Context context) {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();


    }

 /*   public void setLanguageSharedPref(Context context, String lang) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(lang);
        editor.putString("Lang_PREF", json);
        editor.apply();

    }

    public String getLanguageSharedPref(Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String json = sharedPref.getString("Lang_PREF", null);
        Gson gson = new Gson();
        Type type = new TypeToken<String>() {
        }.getType();

        String beans = null;
        try {
            beans = gson.fromJson(json, type);
        } catch (Exception e) {
            return null;
        }
        return beans;
    }
*/

}
