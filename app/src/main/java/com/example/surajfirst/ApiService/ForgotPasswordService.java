package com.example.surajfirst.ApiService;

import android.content.Context;


import com.example.surajfirst.Beans.ForgotPasswordResBean;
import com.example.surajfirst.Interface.ApiResponseListner;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.GoogleProgressDialog;
import com.example.surajfirst.Utils.SuperCastClass;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordService implements Callback<ForgotPasswordResBean> {

    private ApiResponseListner apiResponseListner;
    public GoogleProgressDialog progressDialog;
    private Context context;

    public ForgotPasswordService(Context context, ApiResponseListner apiResponseListner) {
        this.context = context;
        this.apiResponseListner = apiResponseListner;
    }

    public void Connect(Map<String, String> param) {
        try {
        /*    setProgressDialog();
            Log.e("forgot password", "Param >> "+ param.toString());
            String endPoint = EndPointApi.forget_password + "?" + Utils.getStringForGetMethod(param);

            APIInterface apiInterface = APIClient.getClient(context, ApiConstants.GET, endPoint, "").create(APIInterface.class);
            Call<ForgotPasswordResBean> call = (Call<ForgotPasswordResBean>) apiInterface.forgetPassword(param);
            call.enqueue(this);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProgressDialog() {
        progressDialog = new GoogleProgressDialog(context);
        progressDialog.show();
    }


    @Override
    public void onResponse(Call<ForgotPasswordResBean> call, Response<ForgotPasswordResBean> response) {
        ForgotPasswordResBean forgotPasswordResBean = response.body();

        if (forgotPasswordResBean != null) {
            if (forgotPasswordResBean.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
//                SuperCastClass superCastClass = forgotPasswordResBean;
//                apiResponseListner.onSuccess(ForgotPassActivity.forgotPassword, superCastClass);
            } else {
                apiResponseListner.onFailure(forgotPasswordResBean.getMsg());
            }
        } else {

            apiResponseListner.onException("EXCEPTION");
        }
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call<ForgotPasswordResBean> call, Throwable t) {
        progressDialog.dismiss();
        apiResponseListner.onException(t.getMessage());
    }
}
