package com.example.surajfirst.ApiService;

import android.content.Context;
import android.util.Log;

import com.example.surajfirst.Activity.SignupActivity;
import com.example.surajfirst.Beans.SignupResBean;
import com.example.surajfirst.Interface.APIClient;
import com.example.surajfirst.Interface.APIInterface;
import com.example.surajfirst.Interface.ApiResponseListner;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.EndPointApi;
import com.example.surajfirst.Utils.GoogleProgressDialog;
import com.example.surajfirst.Utils.SuperCastClass;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupService implements Callback<SignupResBean> {

    private ApiResponseListner apiResponseListner;
    public GoogleProgressDialog progressDialog;
    private Context context;
//    private APIInterface apiInterface;

    public SignupService(Context context, ApiResponseListner apiResponseListner) {
        this.context = context;
        this.apiResponseListner = apiResponseListner;

//        apiInterface = APIClient.getClient().create(APIInterface.class);
    }
//
//    public void Connect(Map<String, String> headerMap, Map<String, String> stringMap) {
//        setProgressDialog();
//
//        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
//        Call<SignupResBean> call = (Call<SignupResBean>) apiInterface.signup(headerMap, stringMap);
//        call.enqueue(this);
//    }

    public void Connect(JsonObject jsonObject) {
        try {

            setProgressDialog();
            Log.e("signup", "Param >> " + jsonObject.toString());

            APIInterface apiInterface = APIClient.getClient(context, ApiConstants.POST, EndPointApi.singup, jsonObject.toString()).create(APIInterface.class);
//            Call<SignupResBean> call = (Call<SignupResBean>) apiInterface.signup(jsonObject);
//            call.enqueue(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProgressDialog() {
        progressDialog = new GoogleProgressDialog(context);
        progressDialog.show();
    }

    @Override
    public void onResponse(Call<SignupResBean> call, Response<SignupResBean> response) {
        SignupResBean signupResBean = response.body();

        if (signupResBean != null) {
            if (signupResBean.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
                SuperCastClass superCastClass = signupResBean;
                apiResponseListner.onSuccess(SignupActivity.SIGN_UP_TAG, superCastClass);
            } else {
                apiResponseListner.onFailure(signupResBean.getMsg());
            }
        } else {
            apiResponseListner.onException("EXCEPTION");
        }
        progressDialog.dismiss();

    }

    @Override
    public void onFailure(Call<SignupResBean> call, Throwable t) {
        progressDialog.dismiss();
        apiResponseListner.onException(t.getMessage());
    }
}
