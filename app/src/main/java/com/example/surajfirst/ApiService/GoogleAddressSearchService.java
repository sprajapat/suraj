package com.example.surajfirst.ApiService;

import android.content.Context;
import android.util.Log;

import com.example.surajfirst.Beans.GooglePlacesAutoCompleteRes;
import com.example.surajfirst.Interface.APIClient;
import com.example.surajfirst.Interface.APIInterface;
import com.example.surajfirst.Interface.ApiResponseListner;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.GoogleProgressDialog;
import com.example.surajfirst.Utils.SuperCastClass;
import com.google.gson.Gson;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoogleAddressSearchService implements Callback<GooglePlacesAutoCompleteRes> {

    private ApiResponseListner apiResponseListner;
    public GoogleProgressDialog progressDialog;
    private Context context;

    public GoogleAddressSearchService(Context context, ApiResponseListner apiResponseListner) {
        this.context = context;
        this.apiResponseListner = apiResponseListner;
    }

    public void Connect(Map<String, String> param) {
        try {
            setProgressDialog();

            APIInterface apiInterface = APIClient.getClient(context).create(APIInterface.class);
//            Call<GooglePlacesAutoCompleteRes> call = (Call<GooglePlacesAutoCompleteRes>) apiInterface.googleAddressSearch(param);
//            call.enqueue(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProgressDialog() {
        progressDialog = new GoogleProgressDialog(context);
        progressDialog.show();
    }


    @Override
    public void onResponse(Call<GooglePlacesAutoCompleteRes> call, Response<GooglePlacesAutoCompleteRes> response) {
        GooglePlacesAutoCompleteRes googlePlacesAutoCompleteRes = response.body();

        Log.e("response",  " >> "+ new Gson().toJson(googlePlacesAutoCompleteRes));
        if (googlePlacesAutoCompleteRes != null) {
            if (googlePlacesAutoCompleteRes.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
//                SuperCastClass superCastClass = googlePlacesAutoCompleteRes;
//                apiResponseListner.onSuccess(SearchLocationActivity.GooglePlaceAPi, superCastClass);
            } else {
//                apiResponseListner.onFailure(GooglePlacesAutoCompleteRes.getMsg());
            }
        } else {

            apiResponseListner.onException("EXCEPTION");
        }
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call<GooglePlacesAutoCompleteRes> call, Throwable t) {
        progressDialog.dismiss();
        apiResponseListner.onException(t.getMessage());
    }
}
