package com.example.surajfirst.ApiService;

import android.content.Context;

import com.example.surajfirst.Beans.ResondVerifyCodeResBean;
import com.example.surajfirst.Interface.ApiResponseListner;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.GoogleProgressDialog;
import com.example.surajfirst.Utils.SuperCastClass;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResendVerifyCodeService implements Callback<ResondVerifyCodeResBean> {

    private ApiResponseListner apiResponseListner;
    public GoogleProgressDialog progressDialog;
    private Context context;

    public ResendVerifyCodeService(Context context, ApiResponseListner apiResponseListner) {
        this.context = context;
        this.apiResponseListner = apiResponseListner;
    }

    public void resandCode(Map<String, String> param) {
        try {
//            setProgressDialog();
//            Log.e("resend Code", "Param >> "+ param.toString());
//            String endPoint = EndPointApi.resend_verification_code + "?" + Utils.getStringForGetMethod(param);
//
//            APIInterface apiInterface = APIClient.getClient(context, ApiConstants.GET, endPoint, "").create(APIInterface.class);
//            Call<ResondVerifyCodeResBean> call = (Call<ResondVerifyCodeResBean>) apiInterface.resendVerificationCode(param);
//            call.enqueue(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProgressDialog() {
        progressDialog = new GoogleProgressDialog(context);
        progressDialog.show();
    }


    @Override
    public void onResponse(Call<ResondVerifyCodeResBean> call, Response<ResondVerifyCodeResBean> response) {
        ResondVerifyCodeResBean verifyResBean = response.body();

        if (verifyResBean != null) {
            if (verifyResBean.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
                SuperCastClass superCastClass = verifyResBean;
//                apiResponseListner.onSuccess(VerificationActivity.resendCode, superCastClass);
            } else {
                apiResponseListner.onFailure(verifyResBean.getMsg());
            }
        } else {

            apiResponseListner.onException("EXCEPTION");
        }
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call<ResondVerifyCodeResBean> call, Throwable t) {
        progressDialog.dismiss();
        apiResponseListner.onException(t.getMessage());
    }
}
