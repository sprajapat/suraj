package com.example.surajfirst.ApiService;

import android.content.Context;

import com.example.surajfirst.Activity.VerificationActivity;
import com.example.surajfirst.Beans.VerifyResBean;
import com.example.surajfirst.Interface.ApiResponseListner;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.GoogleProgressDialog;
import com.example.surajfirst.Utils.SuperCastClass;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyService implements Callback<VerifyResBean> {

    private ApiResponseListner apiResponseListner;
    public GoogleProgressDialog progressDialog;
    private Context context;

    public VerifyService(Context context, ApiResponseListner apiResponseListner) {
        this.context = context;
        this.apiResponseListner = apiResponseListner;
    }

    public void verifiyCode(Map<String, String> param) {
        try {
           /* setProgressDialog();
            Log.e("verification Code", "Param >> "+ param.toString());
            String endPoint = EndPointApi.verify_verification_code + "?" + Utils.getStringForGetMethod(param);

            APIInterface apiInterface = APIClient.getClient(context, ApiConstants.GET, endPoint, "").create(APIInterface.class);
            Call<VerifyResBean> call = (Call<VerifyResBean>) apiInterface.verifyVerificationCode(param);
            call.enqueue(this);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProgressDialog() {
        progressDialog = new GoogleProgressDialog(context);
        progressDialog.show();
    }


    @Override
    public void onResponse(Call<VerifyResBean> call, Response<VerifyResBean> response) {
        VerifyResBean verifyResBean = response.body();

        if (verifyResBean != null) {
            if (verifyResBean.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
                SuperCastClass superCastClass = verifyResBean;
                apiResponseListner.onSuccess(VerificationActivity.VERIFY_API_TAG, superCastClass);
            } else {
                apiResponseListner.onFailure(verifyResBean.getMsg());
            }
        } else {

            apiResponseListner.onException("EXCEPTION");
        }
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call<VerifyResBean> call, Throwable t) {
        progressDialog.dismiss();
        apiResponseListner.onException(t.getMessage());
    }
}
