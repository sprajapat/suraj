package com.example.surajfirst.ApiService;

import android.content.Context;
import android.util.Log;

import com.example.surajfirst.Activity.LoginActivity;
import com.example.surajfirst.Beans.LoginResBean;
import com.example.surajfirst.Interface.APIClient;
import com.example.surajfirst.Interface.APIInterface;
import com.example.surajfirst.Interface.ApiResponseListner;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.EndPointApi;
import com.example.surajfirst.Utils.GoogleProgressDialog;
import com.example.surajfirst.Utils.SuperCastClass;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginService implements Callback<LoginResBean> {

    private ApiResponseListner apiResponseListner;
    public GoogleProgressDialog progressDialog;
    private Context context;


    public LoginService(Context context, ApiResponseListner apiResponseListner) {

        this.context = context;
        this.apiResponseListner = apiResponseListner;

    }

//    public void Connect(Map<String, String> headerMap,Map<String, String> stringMap) {
//        setProgressDialog();
//        Call<LoginResBean> call = (Call<LoginResBean>) apiInterface.login(headerMap,stringMap);
//        call.enqueue(this);
//    }

    public void Connect(JsonObject jsonObject) {
        try {
            setProgressDialog();

            Log.e("login", "Param >> " + jsonObject.toString());

            APIInterface apiInterface = APIClient.getClient(context, ApiConstants.POST, EndPointApi.login, jsonObject.toString()).create(APIInterface.class);
//            Call<LoginResBean> call = apiInterface.login(jsonObject);
//            call.enqueue(this);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void setProgressDialog() {
        progressDialog = new GoogleProgressDialog(context);
        progressDialog.show();
    }


    @Override
    public void onResponse(Call<LoginResBean> call, Response<LoginResBean> response) {
        LoginResBean loginResBean = response.body();

        if (loginResBean != null) {
            if (loginResBean.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
                SuperCastClass superCastClass = loginResBean;
                apiResponseListner.onSuccess(LoginActivity.LOGIn_API_TAG, superCastClass);
            } else {
                apiResponseListner.onFailure(loginResBean.getMsg());
            }
        } else {
            apiResponseListner.onException("Exception");
        }

        progressDialog.dismiss();
    }

    @Override
    public void onFailure(Call<LoginResBean> call, Throwable t) {
        progressDialog.dismiss();
        apiResponseListner.onException(t.getMessage());


    }
}
