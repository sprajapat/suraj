package com.example.surajfirst.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.surajfirst.Activity.MyOrdersActivity;
import com.example.surajfirst.Activity.PaymentActivity;
import com.example.surajfirst.R;


public class BottomNavFragment extends Fragment implements View.OnClickListener {
    LinearLayout llOrder, llPayBill, llHelp, llWallet, llHome;
    ImageView imgHelp, imgWallet, imgOrder, imgHome;
    FragmentManager manager;
    BottomNavFragment bottomNavFragment;
    AmbulanceBookingFragment ambulanceBookingFragment;
    HomeFragment homeFragment;

    public BottomNavFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.nav_bottom, container, false);
        init(v);

        return v;
    }

    private void init(View v) {

        llOrder = v.findViewById(R.id.llOrder);
        llPayBill = v.findViewById(R.id.llPayBill);
        llWallet = v.findViewById(R.id.llWallet);
        llHelp = v.findViewById(R.id.llHelp);
        llHome = v.findViewById(R.id.llHome);

        imgHome = v.findViewById(R.id.imgHome);
        imgWallet = v.findViewById(R.id.imgWallet);
        imgHelp = v.findViewById(R.id.imgHelp);
        imgOrder = v.findViewById(R.id.imgOrder);

        llHome.setOnClickListener(this);
        llOrder.setOnClickListener(this);
        llPayBill.setOnClickListener(this);
        llHelp.setOnClickListener(this);
        llWallet.setOnClickListener(this);

        imgHome.setImageResource(R.mipmap.homeo);

      /*  manager = getFragmentManager();
        homeFragment = new HomeFragment();
        manager.beginTransaction().replace(R.id.llMainContainer, homeFragment).addToBackStack(null).commit();
*/
    }

    @Override
    public void onClick(View v) {
        imgHome.setImageResource(R.mipmap.home);
        imgOrder.setImageResource(R.mipmap.order);
        imgHelp.setImageResource(R.mipmap.help);
        imgWallet.setImageResource(R.mipmap.wallet);

       /* ((MainActivity) getActivity()).imgBackEliipsis.setImageResource(R.mipmap.ellipsis);
        ((MainActivity) getActivity()).imgSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imgBell.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imgTopLogo.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).txtTopNav.setVisibility(View.GONE);
*/
        switch (v.getId()) {
            case R.id.llHome:
                imgHome.setImageResource(R.mipmap.homeo);
//                manager.beginTransaction().replace(R.id.llMainContainer, homeFragment).addToBackStack(null).commit();
                break;
            case R.id.llOrder:
              /*  ((MainActivity) getActivity()).imgBell.setVisibility(View.GONE);
                ((MainActivity) getActivity()).imgTopLogo.setVisibility(View.GONE);
                ((MainActivity) getActivity()).txtTopNav.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).txtTopNav.setText("My Orders");*/
                imgOrder.setImageResource(R.mipmap.ordero);
                Intent intent = new Intent(getActivity(), MyOrdersActivity.class);
                startActivity(intent);
                break;
            case R.id.llPayBill:
               /* ((MainActivity) getActivity()).imgBell.setVisibility(View.GONE);
                ((MainActivity) getActivity()).imgTopLogo.setVisibility(View.GONE);
                ((MainActivity) getActivity()).txtTopNav.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).txtTopNav.setText("Payment Mode");*/
               intent = new Intent(getActivity(), PaymentActivity.class);
                startActivity(intent);
                break;
            case R.id.llHelp:
              /*  ((MainActivity) getActivity()).imgBell.setVisibility(View.GONE);
                ((MainActivity) getActivity()).imgTopLogo.setVisibility(View.GONE);
                ((MainActivity) getActivity()).txtTopNav.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).txtTopNav.setText("Help");*/
                imgHelp.setImageResource(R.mipmap.helpo);
                break;
            case R.id.llWallet:
              /*  ((MainActivity) getActivity()).imgBell.setVisibility(View.GONE);
                ((MainActivity) getActivity()).imgTopLogo.setVisibility(View.GONE);
                ((MainActivity) getActivity()).txtTopNav.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).txtTopNav.setText("Wallet");*/
                imgWallet.setImageResource(R.mipmap.walleto);
                break;
            default:
                return;
        }
        /*
        if (v == llMyOrders) {
            Intent intent = new Intent(getContext(), MyOrdersActivity.class);
            startActivity(intent);
        } else if (v == llPayBill) {
            Intent intent = new Intent(getActivity(), PaymentActivity.class);
            startActivity(intent);
        } else if (v == llHelp) {
            llHelp.setSelected(!llHelp.isPressed());
            if (llHelp.isPressed()) {
                imgHelp.setImageResource(R.mipmap.helpo);
            } else {
                imgHelp.setImageResource(R.mipmap.help);
            }
        } else if (v == llWallet) {
            llHelp.setSelected(!llWallet.isPressed());
            if (llWallet.isPressed()) {
                imgWallet.setImageResource(R.mipmap.walleto);
            } else {
                imgWallet.setImageResource(R.mipmap.wallet);
            }
        }*/
    }
}
