package com.example.surajfirst.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.Adapter.AmbulanceTypeAdapter;
import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.AmbulanceTypeData;
import com.example.surajfirst.Utils.CommanConstants;
import com.example.surajfirst.Utils.CommenMethods;
import com.example.surajfirst.view.RegularEditText;
import com.example.surajfirst.view.RegularTextView;

import java.util.ArrayList;
import java.util.List;

public class ProceedBookFragment extends Fragment implements View.OnClickListener {
    RegularTextView txtAmbulanceType;
    RegularEditText edtDrop, edtPickup;
    ImageView imgArrowDown, imgWallet, imgCash;
    public LinearLayout llAmbulanceType, llFareEstimate, llNote, llAmbulanceBottom;
    Button btnProceedBook;
    RecyclerView recyclerView;
    LinearLayout.LayoutParams btnparams, ambuparam;
    View v;
    AmbulanceTypeAdapter ambulanceTypeAdapter;
    List<AmbulanceTypeData> ambulanceTypeData = new ArrayList<>();

    public ProceedBookFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_proceedebook, container, false);

        init();
        return v;
    }

    private void init() {

        imgArrowDown = v.findViewById(R.id.imgArrowDown);
        txtAmbulanceType = v.findViewById(R.id.txtAmbulanceType);
        llAmbulanceType = v.findViewById(R.id.llAmbulanceType);
        llFareEstimate = v.findViewById(R.id.llFareEstimate);
        btnProceedBook = v.findViewById(R.id.btnProceedBook);
        llNote = v.findViewById(R.id.llNote);
        edtPickup = v.findViewById(R.id.edtPickup);
        edtDrop = v.findViewById(R.id.edtDrop);
        llAmbulanceBottom = v.findViewById(R.id.llAmbulanceBottom);
        recyclerView = v.findViewById(R.id.rcylrAmbulanceBottom);
        imgCash = v.findViewById(R.id.imgCash);
        imgWallet = v.findViewById(R.id.imgWallet);

        ambulanceTypeAdapter = new AmbulanceTypeAdapter(getContext());   /*, new AmbulanceTypeAdapter.OnItemClickListener() {
            @Override
            public void onShortClick(View view, int position) {
                Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });*/

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ambulanceTypeAdapter.setEventListener(new AmbulanceTypeAdapter.EventListener() {
            @Override
            public void onItemViewClicked(View view, int position) {
                AmbulanceTypeData itemData = ambulanceTypeAdapter.getItemData(position);
          /*      ambulanceTypeAdapter.notifyItemChanged(position);
                ImageView imgRadio = view.findViewById(R.id.imgRadio);
                imgRadio.setImageResource(R.mipmap.radio_checked_b);*/

                ambulanceTypeAdapter.changeSelection(position);
                txtAmbulanceType.setText(ambulanceTypeData.get(position).getAmbulance());
                llAmbulanceType.setVisibility(View.GONE);
                btnProceedBook.setVisibility(View.VISIBLE);
                llFareEstimate.setVisibility(View.VISIBLE);

            }
        });
        prepareData();


        String DROP_TXT = getArguments().getString(CommanConstants.DROP_TXT);
        String PICKUP_TXT = getArguments().getString(CommanConstants.PICKUP_TXT);
        edtDrop.setText(DROP_TXT);
        edtPickup.setText(PICKUP_TXT);

//        btnProceedBook.getBackground().setAlpha(51);

        llAmbulanceType.setVisibility(View.GONE);
        btnProceedBook.setVisibility(View.GONE);
        llFareEstimate.setVisibility(View.GONE);

        imgArrowDown.setOnClickListener(this);
        llAmbulanceType.setOnClickListener(this);
        btnProceedBook.setOnClickListener(this);
        imgWallet.setOnClickListener(this);
        imgCash.setOnClickListener(this);
    }

    private void prepareData() {
        ambulanceTypeData.clear();
        ambulanceTypeData.add(new AmbulanceTypeData("Ambulance type(AC)", R.mipmap.radio_blank));
        ambulanceTypeData.add(new AmbulanceTypeData("Ambulance type(Non-AC)", R.mipmap.radio_blank));
        ambulanceTypeData.add(new AmbulanceTypeData("Ambulance type(Freezer)", R.mipmap.radio_blank));
        ambulanceTypeData.add(new AmbulanceTypeData("Life Support ICU Ambulance", R.mipmap.radio_blank));
        ambulanceTypeAdapter.addAll(ambulanceTypeData);
    }

    @Override
    public void onClick(View v) {
        if (v == imgArrowDown) {
            recyclerView.setAdapter(ambulanceTypeAdapter);

            btnparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            btnparams.setMargins(40, 20, 40, 10);
            btnProceedBook.setLayoutParams(btnparams);
//            btnProceedBook.getBackground().setAlpha(1);
            btnProceedBook.setVisibility(View.GONE);

            llFareEstimate.setVisibility(View.GONE);
            llNote.setVisibility(View.GONE);
            llAmbulanceType.setVisibility(View.VISIBLE);

        }

        /*else if (v == llAmbulanceType) {

            txtAmbulanceType.setText("AMBULACE SELECTED");
            llAmbulanceType.setVisibility(View.GONE);
            llNote.setVisibility(View.VISIBLE);
            llFareEstimate.setVisibility(View.VISIBLE);

            ambuparam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            ambuparam.setMargins(0, 10, 0, 40);
            llAmbulanceBottom.setLayoutParams(ambuparam);

        } */

        else if (v == btnProceedBook) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.llContainer, new PickupFragment());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            llNote.setVisibility(View.VISIBLE);
        } else if (v == imgCash) {

            imgWallet.setImageResource(R.mipmap.radio_blank);
            imgCash.setImageResource(R.mipmap.radio_checked_b);

            CommenMethods.setPref(getActivity(), CommanConstants.PAYMODE, "Cash");


        } else if (v == imgWallet) {
            imgCash.setImageResource(R.mipmap.radio_blank);
            imgWallet.setImageResource(R.mipmap.radio_checked_b);
            CommenMethods.setPref(getActivity(), CommanConstants.PAYMODE, "Wallet");
        }

    }
}

/* if (checkedPosition != getId()) {
            ambulanceTypeAdapter.notifyItemChanged(checkedPosition);
            checkedPosition = ambulanceTypeData.get(position);
        }*/
//                getId().setImageResource(R.mipmap.radio_checked_b);
