package com.example.surajfirst.Fragment;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.surajfirst.Activity.AmbulanceBookingActivity;
import com.example.surajfirst.R;
import com.example.surajfirst.Utils.CommanConstants;
import com.example.surajfirst.view.RegularEditText;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class AmbulanceBookingFragment extends Fragment implements OnMapReadyCallback {
    Button btnReqAmbulance;
    RegularEditText edtPickup, edtDrop;
    ImageView imgLocation;
    GoogleMap mMap;
    Marker marker2, marker1;
    SupportMapFragment smf;
    private static final int REQUEST_LOCATION_PERMISSION = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_ambulance_booking, container, false);
        init(v);

        return v;
    }

    /*all code*/
    private void init(View v) {
        /*all ids are here */
        btnReqAmbulance = v.findViewById(R.id.btnReqAmbulance);
        edtDrop = v.findViewById(R.id.edtDrop);
        edtPickup = v.findViewById(R.id.edtPickup);
        imgLocation = v.findViewById(R.id.imgLocation);


        //Put the value
        final ProceedBookFragment pbf = new ProceedBookFragment();

        /*visibility for first session*/
        btnReqAmbulance.setVisibility(View.GONE);
//        ((AmbulanceBookingActivity)getActivity()).llBottomNav.setVisibility(View.VISIBLE);
        ((AmbulanceBookingActivity)getActivity()).img_back.setVisibility(View.GONE);
        ((AmbulanceBookingActivity)getActivity()).img_ellipsis.setVisibility(View.VISIBLE);
        imgLocation.setVisibility(View.GONE);

        btnReqAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                ((AmbulanceBookingActivity)getActivity()).llBottomNav.setVisibility(View.GONE);

                Bundle args = new Bundle();
                args.putString(CommanConstants.DROP_TXT, edtDrop.getText().toString());
                args.putString(CommanConstants.PICKUP_TXT, edtPickup.getText().toString());
                pbf.setArguments(args);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.llContainer, pbf);
                fragmentTransaction.addToBackStack(null);

                fragmentTransaction.commit();
                ((AmbulanceBookingActivity) getActivity()).llBottom.setVisibility(View.GONE);
            }
        });

        /*set mapView */
        smf = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);

        if (smf == null) {
            FragmentManager manager = getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            smf = SupportMapFragment.newInstance();
            ft.replace(R.id.mapFragment, smf).commit();
        }
        smf.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        /*last location*/
        LatLng home = new LatLng(26.9167, 75.8167);
        marker1 = mMap.addMarker(new MarkerOptions()
                .position(home)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationo)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home, 16));

        /*methods here to call location*/
        enableMyLocation(mMap);
        locationAddress(mMap);
        setMapClick(mMap);
    }

    /*enable location */
    private void enableMyLocation(GoogleMap map) {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
        }

    }

    /*current location*/
    public void locationAddress(final GoogleMap map) {

        if (map.isMyLocationEnabled()) {

            map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location arg0) {

                    LatLng latLng = new LatLng(arg0.getLatitude(), arg0.getLongitude());

                    if (marker1 != null) {
                        marker1.remove();
                    }
                    marker1 = map.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationo)));
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

                    String str = getCompleteAddressString(getActivity(), latLng);
                    edtPickup.setText(str);

                }
            });
        }
    }

    /* destination location */
    private void setMapClick(final GoogleMap map) {

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                String snippet = String.format(Locale.getDefault(), getString(R.string.lat_long_snippet), latLng.latitude, latLng.longitude);
                if (marker2 != null) {
                    marker2.remove();
                }
                marker2 = map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.locationb)));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

                String str = getCompleteAddressString(getActivity(), latLng);
                edtDrop.setText(str);
                btnReqAmbulance.setVisibility(View.VISIBLE);
                imgLocation.setVisibility(View.VISIBLE);

                ((AmbulanceBookingActivity)getActivity()).img_back.setVisibility(View.VISIBLE);
                ((AmbulanceBookingActivity)getActivity()).img_ellipsis.setVisibility(View.GONE);
            }
        });

    }

    /*string address*/
    public static String getCompleteAddressString(Context context, LatLng latLng) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current address", strReturnedAddress.toString());
            } else {
                Log.w("My Current address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current address", "Cannot get Address!");
        }
        return strAdd;
    }

}
