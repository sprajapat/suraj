package com.example.surajfirst.Fragment;

import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.example.surajfirst.R;


public class PickupFragment extends Fragment implements View.OnClickListener {
    View v;
    RelativeLayout RlContainer5, RlContainer7;
    LinearLayout llContainer6, llContainer8, llCancel, viewGroup, llambulanceMain;
    Button btnNo, btnCancel, close;
    Point p;

    public PickupFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_pickup, container, false);
        init();
        return v;
    }

    private void init() {

        RlContainer5 = v.findViewById(R.id.RlContainer5);
        RlContainer7 = v.findViewById(R.id.RlContainer7);
        llContainer6 = v.findViewById(R.id.llContainer6);
        llContainer8 = v.findViewById(R.id.llContainer8);
        llCancel = v.findViewById(R.id.llCancel);

        btnCancel = v.findViewById(R.id.btnCancel);
        btnNo = v.findViewById(R.id.btnNo);

        RlContainer5.setVisibility(View.VISIBLE);
        llContainer6.setVisibility(View.GONE);
        RlContainer7.setVisibility(View.GONE);
        llContainer8.setVisibility(View.GONE);

        llCancel.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnNo.setOnClickListener(this);
        RlContainer7.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == llCancel) {
            RlContainer5.setVisibility(View.GONE);
            llContainer6.setVisibility(View.VISIBLE);
            RlContainer7.setVisibility(View.GONE);
            llContainer8.setVisibility(View.GONE);
        } else if (v == btnCancel) {
            llContainer6.setVisibility(View.GONE);
            showPopup(v);
        } else if (v == btnNo) {
            RlContainer5.setVisibility(View.GONE);
            llContainer6.setVisibility(View.GONE);
            RlContainer7.setVisibility(View.VISIBLE);
            llContainer8.setVisibility(View.GONE);
        }
        else if (v==RlContainer7){
            RlContainer7.setVisibility(View.GONE);
            llContainer8.setVisibility(View.VISIBLE);
        }
    }

    public void showPopup(View anchorView) {

        View popupView = LayoutInflater.from(getActivity()).inflate(R.layout.popup_window_cancel, null);

//        View popupView = getActivity().getLayoutInflater().inflate(R.layout.popup_window_cancel, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(popupView);
        popupWindow.setFocusable(true);
//        popup.setBackgroundDrawable(new ());

        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        popupWindow.showAtLocation(anchorView, Gravity.CENTER,
                0, 0);
        Button btnDone = (Button) popupView.findViewById(R.id.btnDone);
        Button btnClose = (Button) popupView.findViewById(R.id.btnClose);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                llContainer6.setVisibility(View.VISIBLE);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
       /*
        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);
        // Getting a reference to Close button, and close the popup when clicked.
       */
    }

}