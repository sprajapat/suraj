package com.example.surajfirst.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.surajfirst.Activity.AmbulanceBookingActivity;
import com.example.surajfirst.Activity.OffersActivity;
import com.example.surajfirst.Adapter.HomePagerAdapter;
import com.example.surajfirst.R;
import com.example.surajfirst.view.RegularTextView;

import java.util.ArrayList;
import java.util.List;

/*extra fragment not usebale--------------------*/
public class HomeFragment extends Fragment implements View.OnClickListener {
    private ViewPager viewPager;
    RegularTextView txtMoreOffers;
    LinearLayout llAmbulance;
    int images[] = {R.drawable.img2, R.drawable.img3, R.drawable.img1};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home, container, false);

        init(view);
        return view;
    }

    private void init(View view) {

        llAmbulance = view.findViewById(R.id.llAmbulance);
        viewPager = view.findViewById(R.id.viewPager);
        txtMoreOffers = view.findViewById(R.id.txtMoreOffers);

        viewPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        viewPager.setAdapter(new HomePagerAdapter(getActivity(), images));

        txtMoreOffers.setOnClickListener(this);
        llAmbulance.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == llAmbulance) {
            Intent intent = new Intent(getContext(), AmbulanceBookingActivity.class);
            startActivity(intent);
        } else if (v == txtMoreOffers) {
            Intent intent = new Intent(getActivity(), OffersActivity.class);
            startActivity(intent);
        }
    }
}
