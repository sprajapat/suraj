package com.example.surajfirst.SQLDATA;

public class ContactData {

    private int id;
    private String name;
    private String number;
    public boolean isSelected;

    public ContactData() {
    }

    public ContactData(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public ContactData(int id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
