package com.example.surajfirst.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;


/**
 * Created by PIYUSH NAROLA on 24/04/2018.
 */

@SuppressLint("AppCompatCustomView")
public class RegularRadioButton extends RadioButton {

    public RegularRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RegularRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RegularRadioButton(Context context) {
        super(context);
        init();
    }

    private void init() {
        /*Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                GlobalConstant.REGULAR_FONT);
        setTypeface(tf);*/
    }
}
