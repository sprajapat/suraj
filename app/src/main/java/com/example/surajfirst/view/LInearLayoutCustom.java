package com.example.surajfirst.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;

public class LInearLayoutCustom extends FrameLayout {
    Context context;
    private static Paint PAINT = new Paint(Paint.ANTI_ALIAS_FLAG);

    static {
        PAINT.setColor(Color.RED);
    }

    public LInearLayoutCustom(Context context) {
        super(context);
        setWillNotDraw(false);
        init();
    }


    public LInearLayoutCustom(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        init();


    }

    public LInearLayoutCustom(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
        init();

    }

    public LInearLayoutCustom(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setWillNotDraw(false);
        init();

    }

    private void init() {
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius;

        if (width > height) {
            radius = 60;
        } else {
            radius = width / 4;
        }

        Path path = new Path();

        path.addCircle(width / 2,
                height / 2, radius,
                Path.Direction.CW);

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.FILL);

        float center_x, center_y;
        final RectF oval = new RectF();
        paint.setStyle(Paint.Style.STROKE);

        center_x = width / 2;
        center_y = height / 2;

        oval.set(center_x - radius-20,
                0,
                center_x + radius+20,
                radius+radius);
//        canvas.drawLines(pts,0,1,paint);
        canvas.drawArc(oval, 0, -180, false, paint);
        canvas.drawLine(0, radius, center_x - radius-20, radius, paint);
        canvas.drawLine(center_x + radius+20, radius, getWidth(), radius, paint);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        // this gets called, but with a canvas sized after the padding.
    }

}
