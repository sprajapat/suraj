package com.example.surajfirst.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;


@SuppressLint("AppCompatCustomView")
public class BoldRadioButton extends RadioButton {
    public BoldRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BoldRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoldRadioButton(Context context) {
        super(context);
        init();
    }

    private void init() {
       /* Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                GlobalConstant.BOLD_FONT);
        setTypeface(tf);*/
    }
}
