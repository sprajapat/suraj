package com.example.surajfirst.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;


public class CustomView extends View {
    private Paint paint;
    private static final int DEFAULT_CIRCLE_COLOR = Color.RED;

    private int circleColor = DEFAULT_CIRCLE_COLOR;

    public CustomView(Context context) {
        super(context);
        setWillNotDraw(false);
        init(context, null);
    }


    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        init(context, attrs);

    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
        init(context);

    }

    private void init(Context context) {


    }

//    @Override
  /*  protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius;

        if (width > height) {
            radius = 60;
        } else {
            radius = width / 4;
        }

        Path path = new Path();

        path.addCircle(width / 2,
                height / 2, radius,
                Path.Direction.CW);

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.FILL);

        float center_x, center_y;
        final RectF oval = new RectF();
        paint.setStyle(Paint.Style.STROKE);

        center_x = width / 2;
        center_y = height / 2;

        oval.set(center_x - radius - 20,
                0,
                center_x + radius + 20,
                radius + radius);
//        canvas.drawLines(pts,0,1,paint);
        canvas.drawArc(oval, 0, -180, false, paint);
        canvas.drawLine(0, radius, center_x - radius - 20, radius, paint);
        canvas.drawLine(center_x + radius + 20, radius, getWidth(), radius, paint);
    }
*/

    private void init(Context context, AttributeSet attrs) {
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setAntiAlias(true);
    }

    public void setCircleColor(int circleColor) {
        this.circleColor = circleColor;
        invalidate();
    }

    public int getCircleColor() {
        return circleColor;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = getWidth();
        int h = getHeight();

        int pl = getPaddingLeft();
        int pr = getPaddingRight();
        int pt = getPaddingTop();
        int pb = getPaddingBottom();

        int usableWidth = w - (pl + pr);
        int usableHeight = h - (pt + pb);

        int radius = Math.min(usableWidth, usableHeight) / 2;
        int cx = pl + (usableWidth / 2);
        int cy = pt + (usableHeight / 2);

        paint.setColor(circleColor);
        canvas.drawCircle(cx, cy, radius, paint);
    }














   /* private void init(Context context)
    {
//        density = DisplayUtil.getDisplayDensity(context);
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(density * 4);
        //set your own color
        paint.setColor(context.getResources().getColor(R.color.Colorblack));
        path = new Path();
        //array is ON and OFF distances in px (4px line then 2px space)

    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        int measuredHeight = getMeasuredHeight();
        int measuredWidth = getMeasuredWidth();
        if (measuredHeight <= measuredWidth)
        {
            // horizontal
            path.moveTo(0, 0);
            path.lineTo(measuredWidth, 0);
            canvas.drawPath(path, paint);
        }
        else
        {
            // vertical
            path.moveTo(0, 0);
            path.lineTo(0, measuredHeight);
            canvas.drawPath(path, paint);
        }

    }
*/
}

/*
 *//*private void circleDraw(Canvas canvas) {
        int size = 320;
        int radius = (int) (size / 2f);
        // 3
//        canvas.drawCircle(size / 2f, size / 2f, radius, paint);

        // 4
*//**//*
        paint.color = borderColor
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = borderWidth
*//**//*

        // 5
        canvas.drawCircle(size / 2f, size / 2f, radius - paint.getStrokeWidth() / 2f, paint);
    }*//*

    private void lineDraw(Canvas canvas) {
        canvas.drawLine(0, canvas.getHeight() / 4, canvas.getWidth(), canvas.getHeight() / 2, paint);

    }

  *//*  @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size = Math.min(getWidth(),
                getHeight());
        setMeasuredDimension(size, size);
    }*//*

 *//* int mode = MeasureSpec.getMode(widthMeasureSpec);
    int size = MeasureSpec.getSize(widthMeasureSpec);*//*
}*/
