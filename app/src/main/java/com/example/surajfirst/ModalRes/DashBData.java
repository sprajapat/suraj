package com.example.surajfirst.ModalRes;

/*dashboard data------------*/
public class DashBData {
    int imgId;
    String strName;
    public boolean isSelected;


    public DashBData(int imgId, String strName) {
        this.imgId = imgId;
        this.strName = strName;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }
}
