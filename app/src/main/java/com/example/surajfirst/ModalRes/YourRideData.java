package com.example.surajfirst.ModalRes;

public class YourRideData {
    String person;
    String status;
    String amount;
    String payType;
    String date;
    String time;
    int imgId;
    public boolean isMultiSelected;
    public boolean isSelected, select2;

    public YourRideData(String person, String status, String amount, String payType, String date, String time, int imgId) {
        this.person = person;
        this.status = status;
        this.amount = amount;
        this.payType = payType;
        this.imgId = imgId;
        this.time = time;
        this.date = date;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
