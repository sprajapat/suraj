package com.example.surajfirst.ModalRes;

public class AmbulanceTypeData {
    String ambulance;
    int imgId;

    public boolean isSelected;

    public String getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(String ambulance) {
        this.ambulance = ambulance;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public AmbulanceTypeData(String ambulance, int imgId) {
        this.ambulance = ambulance;
        this.imgId = imgId;
    }
}
