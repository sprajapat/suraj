package com.example.surajfirst.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.SkuDetails;
import com.example.surajfirst.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    Context context;
    private List<SkuDetails> skuDetails = new ArrayList<>();
    OnItemClickListener mEventListener;
    BillingClient billingClient;

    public ProductAdapter(Context context) {
        this.context = context;

    }

    public void add(List<SkuDetails> skuDetails, BillingClient billingClient) {
        this.skuDetails = skuDetails;
        this.billingClient = billingClient;
    }

    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_products, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.txtProduct.setText(skuDetails.get(position).getTitle());


        holder.txtProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEventListener != null) {
                    mEventListener.onShortClick(view, position);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return skuDetails.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtProduct;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtProduct = itemView.findViewById(R.id.txtProduct);
        }
    }

    public void setEventListener(OnItemClickListener eventListener) {
        mEventListener = eventListener;
    }

    public interface OnItemClickListener {
        void onShortClick(View view, int position);
    }
}
