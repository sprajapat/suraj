package com.example.surajfirst.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.AmbulanceTypeData;

import java.util.ArrayList;
import java.util.List;

public class AmbulanceTypeAdapter extends RecyclerView.Adapter<AmbulanceTypeAdapter.MyViewHolder> {

    Context context;
    private List<AmbulanceTypeData> ambulanceTypeData = new ArrayList<>();
    EventListener mEventListener;

    public interface EventListener {
        void onItemViewClicked(View view, int position);
    }

    public AmbulanceTypeAdapter(Context context/*, OnItemClickListener listener*/) {
        this.context = context;
    }

    public void addAll(List<AmbulanceTypeData> ambulanceTypeData) {
        try {
            this.ambulanceTypeData.clear();
            this.ambulanceTypeData.addAll(ambulanceTypeData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void changeSelection(int position) {

        for (int i = 0; i < ambulanceTypeData.size(); i++) {
            if (position == i) {
                ambulanceTypeData.get(i).isSelected = true;
            } else {
                ambulanceTypeData.get(i).isSelected = false;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public AmbulanceTypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ambulance_type, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AmbulanceTypeAdapter.MyViewHolder holder, int position) {
//        AmbulanceTypeData data = ambulanceTypeData.get(position);
        holder.bind(ambulanceTypeData.get(position)/*, listener*/);
//        holder.txtAmbulance.setText(data.getAmbulance());
//        holder.imgRadio.setImageResource(data.getImgId());

  /*      holder.llTypeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkedPosition != holder.getAdapterPosition()) {
                    notifyItemChanged(checkedPosition);
                    checkedPosition = holder.getAdapterPosition();
                }
                holder.imgRadio.setImageResource(R.mipmap.radio_checked_b);

                holder.llTypeSelect.setVisibility(View.GONE);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return ambulanceTypeData.size();
    }

    public AmbulanceTypeData getItemData(int position) {
        return ambulanceTypeData.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtAmbulance;
        public ImageView imgRadio;
        public LinearLayout llTypeSelect;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtAmbulance = itemView.findViewById(R.id.txtAmbulance);
//            imgRadio = itemView.findViewById(R.id.imgRadio);
            llTypeSelect = itemView.findViewById(R.id.llTypeSelect);
            this.imgRadio = itemView
                    .findViewById(R.id.imgRadio);
        }

        public void bind(AmbulanceTypeData ambulanceTypeData) {
            txtAmbulance.setText(ambulanceTypeData.getAmbulance());
            imgRadio.setImageResource(ambulanceTypeData.getImgId());

            if (ambulanceTypeData.isSelected) {
                imgRadio.setImageResource(R.mipmap.radio_checked_b);
            } else {
                imgRadio.setImageResource(R.mipmap.radio_blank);
//                llContainer.setBackground(context.getResources().getDrawable(R.drawable.bg_transparent));
            }
     /*       imgRadio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    listener.onShortClick(v, getAdapterPosition());
                    mEventListener.onItemViewClicked(itemView, getAdapterPosition());
                }
            });*/

            llTypeSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    listener.onLongClick(v, getAdapterPosition());
                    mEventListener.onItemViewClicked(itemView, getAdapterPosition());
                }
            });
        }
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

/*    public interface OnItemClickListener {
        void onShortClick(View view, int position);

        void onLongClick(View view, int position);
    }*/
}
