package com.example.surajfirst.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.DashBData;

import java.util.ArrayList;
import java.util.List;

public class DashBAdapter extends RecyclerView.Adapter<DashBAdapter.MyViewHolder> {
    Context context;
    private List<DashBData> dashBdata = new ArrayList<>();
    OnItemclickListener mEventListener;

    public DashBAdapter(Context context) {
        this.context = context;
    }

    public void addAll(List<DashBData> dashBdata) {

        try {
            this.dashBdata.clear();
            this.dashBdata.addAll(dashBdata);

        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void changeSelection(int position) {

        for (int i = 0; i < dashBdata.size(); i++) {
            if (position == i) {
                dashBdata.get(i).isSelected = true;
            } else {
                dashBdata.get(i).isSelected = false;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_dashboard_ryclr, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        DashBData data = dashBdata.get(position);
        holder.txtName.setText(data.getStrName());
        if (data.getImgId() == 0) {

        } else {
            holder.imgProfile.setImageResource(data.getImgId());
        }

//        holder.imgProfile.setImageResource(data.getImgId());
        if (data.isSelected) {
            holder.llContainer.setBackground(context.getResources().getDrawable(R.drawable.bg_btn_primary_5dp));
        } else {
            holder.llContainer.setBackground(context.getResources().getDrawable(R.drawable.bg_transparent));
        }

        holder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onShortClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dashBdata.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgProfile;
        public TextView txtName;
        LinearLayout llContainer;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            txtName = itemView.findViewById(R.id.txtName);
            llContainer = itemView.findViewById(R.id.llContainer);
        }
    }

    public void setEventListener(OnItemclickListener eventListener) {
        mEventListener = eventListener;
    }

    public interface OnItemclickListener {
        void onShortClick(View view, int position);
    }

}
