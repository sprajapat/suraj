package com.example.surajfirst.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.R;
import com.example.surajfirst.SQLDATA.ContactData;
import com.example.surajfirst.SQLDATA.DatabaseHelper;
import com.example.surajfirst.Utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    Context context;
    public List<ContactData> contactData = new ArrayList<>();
    OnItemclickListener mEventListener;
    ;

    public ContactAdapter(Context context) {
        this.context = context;
    }

    public void add(ContactData contact, int pos) {
        try {
            contactData.add(pos, contact);
            Collections.sort(contactData, new Comparator<ContactData>() {
                @Override
                public int compare(ContactData o1, ContactData o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
//            Utils.sortListName(context, contactData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void addAll(List<ContactData> contactData) {

        try {
            this.contactData.clear();
            this.contactData.addAll(contactData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void deleteContacts(int position) {
        (new DatabaseHelper(context)).deleteContact(contactData.get(position));
        contactData.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, contactData.size());
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_data, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ContactData data = contactData.get(position);
        holder.txtName.setText(data.getName());
        holder.txtNumber.setText(data.getNumber());

        holder.llContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onLongClick(v, position);
                }
                return false;
            }
        });
        holder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEventListener.onShortClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgContact;
        public TextView txtName, txtNumber;
        LinearLayout llContainer, llEllipse;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgContact = itemView.findViewById(R.id.imgContact);
            txtName = itemView.findViewById(R.id.txtName);
            txtNumber = itemView.findViewById(R.id.txtContact);
            llContainer = itemView.findViewById(R.id.llContainer);
            llEllipse = itemView.findViewById(R.id.llEllipse);
        }
    }

    public void setEventListener(OnItemclickListener eventListener) {
        mEventListener = eventListener;
    }

    public interface OnItemclickListener {
        void onShortClick(View view, int position);

        void onLongClick(View view, int position);
    }

}
