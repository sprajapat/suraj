package com.example.surajfirst.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.R;


public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyViewHolder> {
    int images[];
    Context context;

    public OffersAdapter(Context context, int images[]) {
        this.context = context;
        this.images = images;
    }

  /*  public void addAll(int images[]) {
        this.images = images;


        notifyDataSetChanged();
    }*/

    /*pending ----------------*/

    @Override
    public OffersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_offers, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OffersAdapter.MyViewHolder holder, int position) {
//        DashBData data = images.get(position);

        holder.imgOffers.setImageResource(images[position]);
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgOffers;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgOffers = itemView.findViewById(R.id.imgOffers);
        }
    }
}
