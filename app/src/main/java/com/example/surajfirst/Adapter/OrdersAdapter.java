package com.example.surajfirst.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.Activity.MyOrdersActivity;
import com.example.surajfirst.Activity.RideDetailActivity;
import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.YourRideData;
import com.example.surajfirst.Utils.CommanConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyViewHolder> {
    Context context;
    private List<YourRideData> yourRideData = new ArrayList<>();
    OnItemClickListener mEventListener;

    LinearLayout.LayoutParams rr1, rr2;
    View view1, view2;
    boolean selected;
    int Count = 0;
    List<Integer> pos = new ArrayList<>();


    public OrdersAdapter(Context context) {
        this.context = context;

    }

    public void addAll(List<YourRideData> yourRideData) {

        try {
            this.yourRideData.clear();
            this.yourRideData.addAll(yourRideData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public YourRideData getItemData(int position) {
        return yourRideData.get(position);
    }


    public int changeSelection(int position) {
        checkCount();
        if (selected) {
            yourRideData.get(position).isSelected = false;
            yourRideData.get(position).isMultiSelected = !yourRideData.get(position).isMultiSelected;
            if (yourRideData.get(position).isMultiSelected) {
                Count++;
            } else {
                Count--;
            }
//            if (yourRideData.get(position).isMultiSelected != true) {
//                yourRideData.get(position).isMultiSelected = true;
//                addPositions(position);
//                Count++;
//            } else {
//                yourRideData.get(position).isMultiSelected = false;
//                deletPositions(position);
//                Count--;
//            }
        } else {

            for (int i = 0; i < yourRideData.size(); i++) {
                if (position == i) {
                    yourRideData.get(i).isSelected = true;
                } else {
                    yourRideData.get(i).isSelected = false;
                }
            }
        }
        notifyDataSetChanged();
        ((MyOrdersActivity) context).txtNumber.setText(String.valueOf(Count));

        return Count;
    }

    private void checkCount() {
        if (Count == 0) {
            selected = false;
        } else {
            selected = true;
        }
    }

    public int changeLongSelection(int position) {
        yourRideData.get(position).isSelected = false;

        yourRideData.get(position).isMultiSelected = !yourRideData.get(position).isMultiSelected;
        if (yourRideData.get(position).isMultiSelected) {
            Count++;
        } else {
            Count--;
        }
        /*if (yourRideData.get(position).isMultiSelected != true) {
            yourRideData.get(position).isMultiSelected = true;
            addPositions(position);
            selected = true;
            Count++;
        } else {
            yourRideData.get(position).isMultiSelected = false;
            deletPositions(position);
            Count--;
        }*/

        notifyDataSetChanged();
        ((MyOrdersActivity) context).txtNumber.setText(String.valueOf(Count));

        return Count;
    }

    public void addPositions(int position) {
        if (pos.contains(position)) {
        } else {
            pos.add(position);
        }
    }

    public void deletPositions(int position) {
        if (pos.contains(position)) {
            for (int i = 0; i < pos.size(); i++) {
                if (position == pos.get(i)) {
                    pos.remove(i);
                }
            }
        } else {
//            pos.add(position);
        }
    }

    public void removeItem(int position) {
        yourRideData.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(YourRideData itemData, int position) {
        yourRideData.add(position, itemData);
        notifyItemInserted(position);
    }

    public int deletMulti() {
        Collections.sort(pos);
//        Collections.reverse(pos);
        if (pos.size() != 0) {
            for (int i = (pos.size() - 1); i >= 0; i--) {
                int k = pos.get(i);
                yourRideData.remove(k);
                notifyItemRemoved(k);
                notifyItemRangeChanged(k, yourRideData.size());
            }
        }
        Log.e("pos have", String.valueOf(pos));
        pos.clear();
        Count = 0;
        notifyDataSetChanged();
        selected = false;
        return pos.size();

    }


    @Override
    public OrdersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_your_ride, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        holder.bind(yourRideData.get(position)/*, listener*/);

        final YourRideData data = yourRideData.get(position);
        String str = "canceled";

        holder.imgPerson.setImageResource(data.getImgId());
        holder.txtPerson.setText(data.getPerson());
        holder.txtAmount.setText(data.getAmount());

        if (data.getStatus().toLowerCase().equalsIgnoreCase(str.toLowerCase())) {
            holder.txtStatus.setText(data.getStatus());
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.colorRed));

        } else {
            holder.txtStatus.setText(data.getStatus());
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.colorDarkGreen));
        }
        holder.txtPayType.setText(data.getPayType());
        holder.txtTime.setText(data.getTime());
        holder.txtDate.setText(data.getDate());


        if (data.isMultiSelected) {
            addPositions(position);
            selected = true;
            holder.imgClick.setVisibility(View.VISIBLE);
            holder.llRideDetail.setBackgroundColor(context.getResources().getColor(R.color.colorbg));
        } else {
            deletPositions(position);
            holder.imgClick.setVisibility(View.GONE);
            holder.llRideDetail.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
        }
        if (data.isSelected) {
            Intent intent = new Intent(context, RideDetailActivity.class);
            intent.putExtra(CommanConstants.PERSON, data.getPerson());
            intent.putExtra(CommanConstants.TIME, data.getTime());
            intent.putExtra(CommanConstants.DATE, data.getDate());
            intent.putExtra(CommanConstants.STATUS, data.getStatus());
            intent.putExtra(CommanConstants.AMOUNT, data.getAmount());
            intent.putExtra(CommanConstants.PAYTYPE, data.getPayType());
            Bundle bundle = new Bundle();
            bundle.putInt(CommanConstants.IMAGEID, data.getImgId());
            intent.putExtras(bundle);
            context.startActivity(intent);
        } else {

        }


        holder.llRideDetail.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                if (mEventListener != null) {
                    holder.llRideDetail.setOnClickListener(null);
                    mEventListener.onLongClick(view, position);
                }
                return true;
            }
        });

        holder.llRideDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEventListener != null) {
                    mEventListener.onShortClick(view, position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return yourRideData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTime, txtPerson, txtDate, txtStatus, txtPayType, txtAmount;
        ImageView imgPerson, imgClick;
        FrameLayout llRideDetail;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgPerson = itemView.findViewById(R.id.imgPerson);
            imgClick = itemView.findViewById(R.id.imgClick);
            txtPerson = itemView.findViewById(R.id.txtPerson);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            txtPayType = itemView.findViewById(R.id.txtPayType);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtTime = itemView.findViewById(R.id.txtTime);
            llRideDetail = itemView.findViewById(R.id.llRideDetail);
        }


//        public void bind(final YourRideData yourRideData) {
//
//        }
    }

    public void setEventListener(OnItemClickListener eventListener) {
        mEventListener = eventListener;
    }

    public interface OnItemClickListener {
        void onShortClick(View view, int position);

        void onLongClick(View view, int position);

        /*void onMoved(RecyclerView.ViewHolder view, int position);

        void onSwiped(RecyclerView.ViewHolder view, int position);
    }*/


    }
}
