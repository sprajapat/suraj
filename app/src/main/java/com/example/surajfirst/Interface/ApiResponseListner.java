package com.example.surajfirst.Interface;

import com.example.surajfirst.Utils.SuperCastClass;

/**
 * Created by admin on 9/7/2017.
 */

public interface ApiResponseListner {

    public void onSuccess(String Tag, SuperCastClass superCastClass);
    public void onFailure(String message);
    public void onException(String message);

}
