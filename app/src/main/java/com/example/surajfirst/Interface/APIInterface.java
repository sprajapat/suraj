package com.example.surajfirst.Interface;


import com.example.surajfirst.Beans.LoginResBean;
import com.example.surajfirst.IntRes.LoginRes;
import com.example.surajfirst.Utils.ApiConstants;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by admin on 9/6/2017.
 */

public interface APIInterface {

    @POST(ApiConstants.LOGIN_URL)
        //  DONE
    Call<LoginRes> login(@Body JsonObject jsonObject);

    @POST(ApiConstants.SIGNUP_URL)
//  DONE
    Call<Object> signup(@Body JSONObject jsonObject);


}
