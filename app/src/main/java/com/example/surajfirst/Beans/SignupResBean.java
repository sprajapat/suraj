package com.example.surajfirst.Beans;


import com.example.surajfirst.Utils.SuperCastClass;

public class SignupResBean extends SuperCastClass {

    /*status : error
    msg : Forbidden -- Api key required.
*/
    private String status;
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
