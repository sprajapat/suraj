package com.example.surajfirst.Beans;


import com.example.surajfirst.Utils.SuperCastClass;

public class ForgotPasswordResBean extends SuperCastClass {
    private String status;
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
