package com.example.surajfirst.Beans;


import com.example.surajfirst.Utils.SuperCastClass;

public class LoginResBean extends SuperCastClass {


    /**
     * status : success
     * msg : You are now logged in!
     * data : {"id":60,"is_active":1,"is_mobile_verified":1,"is_email_verified":1,"name":"abnersmith250","email":"abnersmith250@mailinator.com","phone_number":"8080808080","user_role_id":3,"is_required_update_password":0,"is_online":"online"}
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU0OTQwNjU4MzFjOWQ4NmI1MjZlMDgzOTMwMWU2MDI3ODkyNDQ2MzNjYzI2MWQxZTE3MGZhN2E5M2I3MjI2YjZlMGUyMjg4OTliM2U3YWJiIn0.eyJhdWQiOiIyIiwianRpIjoiZTQ5NDA2NTgzMWM5ZDg2YjUyNmUwODM5MzAxZTYwMjc4OTI0NDYzM2NjMjYxZDFlMTcwZmE3YTkzYjcyMjZiNmUwZTIyODg5OWIzZTdhYmIiLCJpYXQiOjE1Nzk3NjQ3MDcsIm5iZiI6MTU3OTc2NDcwNywiZXhwIjoxNjExMzg3MTA3LCJzdWIiOiI2MCIsInNjb3BlcyI6W119.Ffw_LtEnh_JfbQVjbMnobVGu9_tURg0Gw5dN6pzyip2I0jy7ihWROXq8jNN6A9XWwF2NWYsc_7pv3BrwkkxDP1xVFMFdN6vdoiaF5GiekwniGIJjp0OaDRCS067V8NgLvy0cq_fzxmIYmUxPw2hrnBFAnxweP51l3ApJgq9rQ9v1erN_ZnpgW7xTb8NHr67SgnZJgkedk-_J4zkdRLC02jCKIPqhBxQhhIbGF7DPxMm0o6FPHdd2jX7U9xGplwkhYBIJ6h98--iCT2213P5Yl5Cf0D7ZXD2ClpkVdDts61IyULGhmuKjRiKgN6Lq0ZeiVYR-l0TPmvXNl2Jh4anG1RxVcPrc9ZIuPeiv2qMBiCxENXbaHVhpeoU5dJo9TJuqhSE40uKkVICTG8OpYy40zDM21cb7Yy9VDRIwtm511-EE5b3NnGZ25awYwlCkIgIX-CjdU64wgNkRza7SMiLlq6G2l_BMkO6Cgum_w2OItyrjgQx5oy8J8Nrvz-sw62EKNDhmg52VR-Ci-bSyN38VjkzZeUrdFazqEpy65T4p0w9k9R05TZr3oFoXhygkr48l9Q74wBXY1P7QCrU0J2hATBkqDvcW21KTb2uyiD4rC1z4sAAw_FVQaMOYDDFOyTdA0KkEf9Jc6xpHRFuX_nvyNxNs4OJclW8xcmd28RDIZW0
     */

    private String status;
    private String msg;
    private DataBean data;
    private String token;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static class DataBean {
        /**
         * id : 60
         * is_active : 1
         * is_mobile_verified : 1
         * is_email_verified : 1
         * name : abnersmith250
         * email : abnersmith250@mailinator.com
         * phone_number : 8080808080
         * user_role_id : 3
         * is_required_update_password : 0
         * is_online : online
         */

        private int id;
        private int is_active;
        private int is_mobile_verified;
        private int is_email_verified;
        private String name;
        private String email;
        private String phone_number;
        private int user_role_id;
        private int is_required_update_password;
        private String is_online;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public int getIs_mobile_verified() {
            return is_mobile_verified;
        }

        public void setIs_mobile_verified(int is_mobile_verified) {
            this.is_mobile_verified = is_mobile_verified;
        }

        public int getIs_email_verified() {
            return is_email_verified;
        }

        public void setIs_email_verified(int is_email_verified) {
            this.is_email_verified = is_email_verified;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public int getUser_role_id() {
            return user_role_id;
        }

        public void setUser_role_id(int user_role_id) {
            this.user_role_id = user_role_id;
        }

        public int getIs_required_update_password() {
            return is_required_update_password;
        }

        public void setIs_required_update_password(int is_required_update_password) {
            this.is_required_update_password = is_required_update_password;
        }

        public String getIs_online() {
            return is_online;
        }

        public void setIs_online(String is_online) {
            this.is_online = is_online;
        }
    }
}
