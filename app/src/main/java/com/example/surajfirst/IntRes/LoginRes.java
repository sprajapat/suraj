package com.example.surajfirst.IntRes;

import com.google.gson.Gson;

public class LoginRes {


    /**
     * status : success
     * msg : You are now logged in!
     * data : {"id":27,"is_active":1,"is_mobile_verified":1,"is_email_verified":1,"name":"Robert","email":"robertdowney@abyssmail.com","phone_number":"8058964777","user_role_id":2,"is_required_update_password":1}
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFlYjdlZjBhNzU1ODc1Y2Q4ZDQ0NDZjZTI2MGM4ZTVkM2RjMjQ4OGNkOTg2ZDAxNjFlNGNjMzBlYzE2ZjgwOTM3Yjc5MTgzMGQ3MjdkZTRlIn0.eyJhdWQiOiIyIiwianRpIjoiYWViN2VmMGE3NTU4NzVjZDhkNDQ0NmNlMjYwYzhlNWQzZGMyNDg4Y2Q5ODZkMDE2MWU0Y2MzMGVjMTZmODA5MzdiNzkxODMwZDcyN2RlNGUiLCJpYXQiOjE1ODA3MjgwMzAsIm5iZiI6MTU4MDcyODAzMCwiZXhwIjoxNjEyMzUwNDMwLCJzdWIiOiIyNyIsInNjb3BlcyI6W119.j8rtirmAC0FEntDqBZ81yXa-bmxsKGbOhuy1cC7xk9sAzNojIDHQAzRF9AbZLUJQPeJAKkhYXLOt-mtllCEdkwqsiI4XZm7yejg4rfii36zH-MWrHVss_yOYx-bniSsgBs-An_5ND4NJgzKQD0EHeXnvmcWVuB56lY2-uQ0x92My--t6VI5jV4QCJ5_N9C4JnJpLA6pAndvVeVO6CcLrO9whe8_1kkyhwlHYibn1SLhWd5NHNHwMRHahJg8vaG84Kh9H9pWzAfvIyoNmsqkvdiGjxjvVrzvjdDj9xVFMY0m2GM7ZB3wU28hq4ZVuiubMLcvuE6flO_NDRZ5Bab0uxuiqENeSvQ2cWl6P_e8Z-3rCpDlT3HXVCWEL10YA0y9Xc8VGiKvtB3wmJHUr7Qk7qcQ5dlooHihw7aJsPIOUmYwwZ1VDAFadUxokRFhnwcochI3qvssCjjv5vvAnon_fKTWFOvrN-HYEctkoL601tol4E1PFVnS1h54KemjcTskwT5berw8ze8h3hnDA1WkjRgYu-yKpPfW0OCIXAn8FkB13OI6dvSx8rULS6JCiV8sGJpsp-_0uXRrZ42kVxQTooeotP8NiLRSlC26I5kc7_s2xr3waJsNJFQJquJjfJd_WLdPclCtIFY4iJNoKM3pJnYQwRf4zXZXVU7F2-dF3MN4
     */

    private String status;
    private String msg;
    private DataBean data;
    private String token;

    public static LoginRes objectFromData(String str) {

        return new Gson().fromJson(str, LoginRes.class);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static class DataBean {
        /**
         * id : 27.0
         * is_active : 1.0
         * is_mobile_verified : 1.0
         * is_email_verified : 1.0
         * name : Robert
         * email : robertdowney@abyssmail.com
         * phone_number : 8058964777
         * user_role_id : 2.0
         * is_required_update_password : 1.0
         */

        private double id;
        private double is_active;
        private double is_mobile_verified;
        private double is_email_verified;
        private String name;
        private String email;
        private String phone_number;
        private double user_role_id;
        private double is_required_update_password;

        public static DataBean objectFromData(String str) {

            return new Gson().fromJson(str, DataBean.class);
        }

        public double getId() {
            return id;
        }

        public void setId(double id) {
            this.id = id;
        }

        public double getIs_active() {
            return is_active;
        }

        public void setIs_active(double is_active) {
            this.is_active = is_active;
        }

        public double getIs_mobile_verified() {
            return is_mobile_verified;
        }

        public void setIs_mobile_verified(double is_mobile_verified) {
            this.is_mobile_verified = is_mobile_verified;
        }

        public double getIs_email_verified() {
            return is_email_verified;
        }

        public void setIs_email_verified(double is_email_verified) {
            this.is_email_verified = is_email_verified;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public double getUser_role_id() {
            return user_role_id;
        }

        public void setUser_role_id(double user_role_id) {
            this.user_role_id = user_role_id;
        }

        public double getIs_required_update_password() {
            return is_required_update_password;
        }

        public void setIs_required_update_password(double is_required_update_password) {
            this.is_required_update_password = is_required_update_password;
        }
    }
}
