package com.example.surajfirst;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    String title, text;
    TextView txttitle, txttext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        txttitle = findViewById(R.id.title);
        txttext = findViewById(R.id.text);

        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.cancel(0);

        Intent i = getIntent();
        title = i.getStringExtra("title");
        text = i.getStringExtra("text");

        txttitle.setText(title);
        txttext.setText(text);
    }
}
