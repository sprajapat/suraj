package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surajfirst.R;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout btn_changepass;
    ImageView img_menu, img_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
    }

    public void init() {
        img_menu = findViewById(R.id.img_menu);
        img_edit = findViewById(R.id.img_edit);
        btn_changepass = findViewById(R.id.btn_changepass);

        img_edit.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        btn_changepass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_changepass) {
            Intent intent = new Intent(ProfileActivity.this, ChangePassActivity.class);
            startActivity(intent);
            // finish();
        }
        if (v == img_menu) {

        }
        if (v == img_edit) {
            Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
            startActivity(intent);
        }
    }
}
