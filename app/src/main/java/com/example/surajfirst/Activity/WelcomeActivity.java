package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surajfirst.R;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener{
    Button btn_start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        findViewById();
        init();
    }

    public void findViewById() {
        btn_start = findViewById(R.id.btn_start);

    }

    public void init() {
        btn_start.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_start) {
            Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
            startActivity(intent);
           // finish();
        }
    }
}
