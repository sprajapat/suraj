package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.surajfirst.Adapter.DashBAdapter;
import com.example.surajfirst.Adapter.HomePagerAdapter;
import com.example.surajfirst.Fragment.BottomNavFragment;
import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.DashBData;
import com.example.surajfirst.view.RegularTextView;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager viewPager;
    RegularTextView txtMoreOffers;
    BottomNavFragment bottomNavFragment;
    View view;
    public SlidingRootNav slidingRootNav;
    ImageView img_ellipsis;
    FragmentManager manager;
    LinearLayout llAmbulance, llFood;
    RecyclerView recyclerView;

    private List<DashBData> dashBdata = new ArrayList<>();
    private DashBAdapter dashBAdapter;
    int images[] = {R.drawable.img1, R.drawable.img2, R.drawable.img3};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init(savedInstanceState);
    }

    public void init(Bundle savedInstanceState) {
        llAmbulance = findViewById(R.id.llAmbulance);
        llFood = findViewById(R.id.llFood);
        viewPager = findViewById(R.id.viewPager);
        txtMoreOffers = findViewById(R.id.txtMoreOffers);
        img_ellipsis = findViewById(R.id.img_ellipsis);

        viewPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        viewPager.setAdapter(new HomePagerAdapter(this, images));

        txtMoreOffers.setOnClickListener(this);
        llAmbulance.setOnClickListener(this);
        llFood.setOnClickListener(this);
        img_ellipsis.setOnClickListener(this);

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withDragDistance(150) //Horizontal translation of a view. Default == 180dp
                .withRootViewScale(0.7f) //Content view's scale will be interpolated between 1f and 0.7f. Default == 0.65f;
                .withRootViewElevation(10) //Content view's elevation will be interpolated between 0 and 10dp. Default == 8.
                .withRootViewYTranslation(3)
                .withMenuLayout(R.layout.drawer_nav_menu)
                .inject();


        manager = getSupportFragmentManager();

        bottomNavFragment = new BottomNavFragment();
        manager.beginTransaction().replace(R.id.dynamic_content, bottomNavFragment).commit();

        recyclerView = findViewById(R.id.recyclerdashB);
        dashBAdapter = new DashBAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(dashBAdapter);
        prepareData();
    }

    private void prepareData() {
        dashBdata.clear();
        dashBdata.add(new DashBData(R.mipmap.profile, "MyProfile"));
        dashBdata.add(new DashBData(R.mipmap.offer, "Offers"));
        dashBdata.add(new DashBData(R.mipmap.my_order, "My Orders"));
        dashBdata.add(new DashBData(R.mipmap.notification, "Notifications"));
        dashBdata.add(new DashBData(R.mipmap.support, "Support"));
        dashBdata.add(new DashBData(R.mipmap.rate, "Rate Us"));
        dashBdata.add(new DashBData(R.mipmap.setting, "Settings"));

        dashBAdapter.addAll(dashBdata);
    }

    @Override
    public void onClick(View v) {
        if (v == llAmbulance) {
            Intent intent = new Intent(HomeActivity.this, AmbulanceBookingActivity.class);
            startActivity(intent);
        } else if (v == llFood) {
            Intent intent = new Intent(HomeActivity.this, ContactActivity.class);
            startActivity(intent);
        } else if (v == txtMoreOffers) {
            Intent intent = new Intent(HomeActivity.this, OffersActivity.class);
            startActivity(intent);
        } else if (v == img_ellipsis) {
            slidingRootNav.openMenu();
        }
    }
}
