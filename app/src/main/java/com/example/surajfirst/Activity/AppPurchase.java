package com.example.surajfirst.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.example.surajfirst.Adapter.ProductAdapter;
import com.example.surajfirst.R;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.Utils;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class AppPurchase extends AppCompatActivity {
    BillingClient mBillingClient;
    RecyclerView recyclerView;
    ProductAdapter productAdapter;

    static final String ITEM_SKU = "com.example.button1";
    private Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_purchase);
        init();
    }

    private void init() {
        button1 = findViewById(R.id.button3);
        recyclerView = findViewById(R.id.recyclerView);


        mBillingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {

            }
        }).build();

        mBillingClient.startConnection(new BillingClientStateListener() {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The billing client is ready. You can query purchases here.
                    Utils.showToast(getApplicationContext(), "Succes to connect");
                } else {
                    Utils.showToast(getApplicationContext(), "" + billingResult);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                Utils.showToast(getApplicationContext(), "Disconnect");
            }
        });


        final List<String> skuList = new ArrayList<>();
        skuList.add("premium_upgrade");
        skuList.add("gas");


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBillingClient.isReady()) {

                    SkuDetailsParams params = SkuDetailsParams.newBuilder().setSkusList(skuList).setType(BillingClient.SkuType.INAPP).build();

                    mBillingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                        @Override
                        public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {

                                for (SkuDetails skuDetails : list) {
                                    String sku = skuDetails.getSku();
                                    String price = skuDetails.getPrice();
                                    if ("premium_upgrade".equals(sku)) {
                                        String premiumUpgradePrice = price;
                                    } else if ("gas".equals(sku)) {
                                        String gasPrice = price;
                                    }
                                }

                           /*     BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                        .setSkuDetails(skuDetails)
                                        .build();
                                int responseCode = billingClient.launchBillingFlow(flowParams);*/
                            } else {
                                Utils.showToast(getApplicationContext(), "cannot query product");
                            }
                        }
                    });

                } else {
                    Utils.showToast(getApplicationContext(), "Billing Client is not ready");
                }
            }
        });

        productAdapter = new ProductAdapter(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        productAdapter.setEventListener(new ProductAdapter.OnItemClickListener() {
            @Override
            public void onShortClick(View view, int position) {


                BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails((SkuDetails) skuList).build();
                mBillingClient.launchBillingFlow(AppPurchase.this, billingFlowParams);
            }
        });
    }

    private void loadProduct(List<SkuDetails> skuDetailsList) {
        productAdapter.add(skuDetailsList, mBillingClient);
        recyclerView.setAdapter(productAdapter);

    }

}
