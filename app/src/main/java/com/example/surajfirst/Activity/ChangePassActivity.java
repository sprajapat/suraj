package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surajfirst.R;
import com.example.surajfirst.view.RegularEditText;
import com.example.surajfirst.view.RegularTextView;

public class ChangePassActivity extends AppCompatActivity implements View.OnClickListener {
    //fields are defined
    private RegularTextView tv_save;
    private RegularEditText ev_changepass, ev_changenewpass, ev_changeconfirmpass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        //init function created for other code than onCrate method
        init();
    }

    public void init() {
        //all ids are fetched of Textvies & editViews
        tv_save = findViewById(R.id.tv_save);
        ev_changepass = findViewById(R.id.ev_changepass);
        ev_changenewpass = findViewById(R.id.ev_changenewpass);
        ev_changeconfirmpass = findViewById(R.id.ev_changeconfirmpass);
        //setup all click events here
        tv_save.setOnClickListener(this);
    }


    /// all click events definition here
    @Override
    public void onClick(View v) {
        if (v == tv_save) {
            if (TextUtils.isEmpty(ev_changepass.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(ev_changenewpass.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter email", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(ev_changeconfirmpass.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter phone number", Toast.LENGTH_SHORT).show();
            } else if (!ev_changeconfirmpass.getText().toString().trim().matches(ev_changenewpass.getText().toString().trim())) {
                Toast.makeText(getApplicationContext(), "mismatch password", Toast.LENGTH_SHORT).show();
            } else {
                // call to LoginActivity
                Intent intent = new Intent(ChangePassActivity.this, LoginActivity.class);
                startActivity(intent);
                //finish();
            }
        }//close tv_save click event
    }//close all click events
}