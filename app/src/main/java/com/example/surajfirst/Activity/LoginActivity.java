package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.surajfirst.IntRes.LoginRes;
import com.example.surajfirst.Interface.APIClient;
import com.example.surajfirst.Interface.APIInterface;
import com.example.surajfirst.R;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.CommanConstants;
import com.example.surajfirst.Utils.CommenMethods;
import com.example.surajfirst.Utils.EndPointApi;
import com.example.surajfirst.Utils.NetworkCheck;
import com.example.surajfirst.Utils.Notification;
import com.example.surajfirst.Utils.Utils;
import com.example.surajfirst.view.RegularEditText;
import com.example.surajfirst.view.RegularTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private RegularTextView tv_signup, tv_forgotpass;
    private RegularEditText edtEmail, edtPassword;
    private LinearLayout llLogin;
    ProgressBar progressBar;
    public static String LOGIn_API_TAG = "login";
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        init();
    }

    private void init() {

        tv_signup = findViewById(R.id.tv_signup);
        tv_forgotpass = findViewById(R.id.tv_forgotpass);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        llLogin = findViewById(R.id.llLogin);
        progressBar = findViewById(R.id.progressBar);

        tv_forgotpass.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        llLogin.setOnClickListener(this);
        Notification.CustomNotification(getApplicationContext(), getPackageName(), R.layout.custom_notification, "Android", "Hiiii There", "Hello");
//        Notification.DefaultNotification(getApplicationContext(), "Android", "Hiiii There", "Hello");
    }

    @Override
    public void onClick(View v) {
        if (v == tv_signup) {
            Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
            startActivity(intent);
            //finish();
        } else if (v == tv_forgotpass) {
            Intent intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
            startActivity(intent);
            //finish();
        } else if (v == llLogin) {
            if (TextUtils.isEmpty(edtEmail.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter email address", Toast.LENGTH_SHORT).show();
            } else if (!edtEmail.getText().toString().trim().matches(CommanConstants.emailPattern)) {
                Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(edtPassword.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_SHORT).show();
            } else {
                progressBar.setVisibility(View.VISIBLE);
//              userLogin();
                try {
                    login();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void login() throws IOException {
        if (NetworkCheck.isConnected(this)) {

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(ApiConstants.EMAIl, edtEmail.getText().toString().trim());
            jsonObject.addProperty(ApiConstants.PASSWORD, edtPassword.getText().toString().trim());
            jsonObject.addProperty(ApiConstants.DEVICE_ID, Utils.getDeviceId(LoginActivity.this));
            jsonObject.addProperty(ApiConstants.DEVICE_TYPE, ApiConstants.android);

            APIInterface apiInterface = APIClient.getClient(getApplicationContext(), ApiConstants.POST, EndPointApi.login, jsonObject.toString()).create(APIInterface.class);

            Call<LoginRes> call = apiInterface.login(jsonObject);

            call.enqueue(new Callback<LoginRes>() {
                @Override
                public void onResponse(Call<LoginRes> call, retrofit2.Response<LoginRes> response) {
                    progressBar.setVisibility(View.GONE);
                    LoginRes loginRes = (LoginRes) response.body();
                    if (loginRes != null) {
                        if (loginRes.getStatus().equalsIgnoreCase(ApiConstants.SUCCESS)) {
                            Toast.makeText(getApplicationContext(), loginRes.getMsg(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), loginRes.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            CommenMethods.showToast(this, getResources().getString(R.string.Internet_connectivity));
        }

    }

    private void userLogin() {

        if (NetworkCheck.isConnected(this)) {
            final String currentTimeStemp = Utils.getCurrentTimeStemp();

            ApiConstants.ANDROID_DEVICE_ID = "1234";

            final Map<String, String> stringMap = new HashMap<>();
            stringMap.put(ApiConstants.DEVICE_ID, ApiConstants.ANDROID_DEVICE_ID);
            stringMap.put(ApiConstants.DEVICE_TYPE, "android");
            stringMap.put(ApiConstants.EMAIl, edtEmail.getText().toString().trim());
            stringMap.put(ApiConstants.PASSWORD, edtPassword.getText().toString().trim());

            final JSONObject jString = new JSONObject(stringMap);

            final String signature = Utils.encodeSha56(currentTimeStemp + CommanConstants.POST_Method + EndPointApi.login + jString.toString());

            final Map<String, String> headerMap = new HashMap<>();
            headerMap.put(ApiConstants.Authorization, Utils.getUserToken(getApplicationContext()));
            headerMap.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);
            headerMap.put(ApiConstants.API_TIMESTAMP, currentTimeStemp);
            headerMap.put(ApiConstants.API_SECRET, signature);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    ApiConstants.URL_LOGIN, jString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String str = response.toString();
                    Log.e("login response", " >> " + response.toString());
                    try {
                        String msg = response.getString("msg");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                    startActivity(new Intent(getApplicationContext(), ProfileActivity.class));

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    error.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headerMap;
                }
            };

            VolleySingleton volleySingleton = VolleySingleton.getInstance(this);
            volleySingleton.addToRequestQueue(jsonObjectRequest);

        } else {
            CommenMethods.showToast(this, getResources().getString(R.string.Internet_connectivity));
        }


    }

}