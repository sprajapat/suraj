package com.example.surajfirst.Activity;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.surajfirst.Fragment.AmbulanceBookingFragment;
import com.example.surajfirst.Fragment.BottomNavFragment;
import com.example.surajfirst.Fragment.ProceedBookFragment;
import com.example.surajfirst.R;

public class AmbulanceBookingActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnCancel, btnNo, close, btnReqAmbulance;
    public ImageView img_ellipsis, img_back;
    Point p;
    Activity activity;
    public LinearLayout llcontainer, llBottom;
    public FrameLayout llBottomNav;
    View view;
    FragmentManager manager;
    AmbulanceBookingFragment ambulanceBookingFragment;
    BottomNavFragment bottomNavFragment;
    ProceedBookFragment proceedBookFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_booking);
        init();

    }

    private void init() {
        llcontainer = findViewById(R.id.llContainer);
        img_back = findViewById(R.id.img_back);
        img_ellipsis = findViewById(R.id.img_ellipsis);
        llBottom = findViewById(R.id.llBottom);


        manager = getSupportFragmentManager();
        ambulanceBookingFragment = new AmbulanceBookingFragment();
        manager.beginTransaction().replace(R.id.llContainer, ambulanceBookingFragment).commit();

        bottomNavFragment = new BottomNavFragment();
        manager.beginTransaction().replace(R.id.llBottom, bottomNavFragment).commit();

  /*      LinearLayout dynamicContent = (LinearLayout) findViewById(R.id.dynamic_content);
        View bottom_view = getLayoutInflater()
                .inflate(R.layout.nav_bottom, dynamicContent, false);
        dynamicContent.addView(bottom_view);
        llBottomNav = findViewById(R.id.llBottomNav);
        llBottomNav.setVisibility(View.VISIBLE);*/
    }

    @Override
    public void onClick(View v) {

    }
}

