package com.example.surajfirst.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.surajfirst.Adapter.ContactAdapter;
import com.example.surajfirst.R;
import com.example.surajfirst.SQLDATA.ContactData;
import com.example.surajfirst.SQLDATA.DatabaseHelper;
import com.example.surajfirst.Utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ContactAdapter contactAdapter;
    DatabaseHelper db;
    List<ContactData> contactData = new ArrayList<>();
    EditText edtNumber, edtName;
    TextView btnAdd;
    FloatingActionButton fab;
    int select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        init();
    }

    private void init() {
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);

        JsonObject student = new JsonObject();
        student.addProperty("TABLE_NAME", "SurajDb");
        student.addProperty("COLUMN_ID", "id");
        student.addProperty("COLUMN_NAME", "name");
        student.addProperty("COLUMN_NUMBER", "number");
        try {
            Utils.createTables(student);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        contactAdapter = new ContactAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(contactAdapter);

        contactAdapter.setEventListener(new ContactAdapter.OnItemclickListener() {
            @Override
            public void onShortClick(View view, int position) {
                select = 1;
                showDialog(view, position);
            }

            @Override
            public void onLongClick(View view, int position) {
                contactAdapter.deleteContacts(position);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select = 0;
                showDialog(v, 5);
            }
        });


        db = new DatabaseHelper(getApplicationContext());
//        db.deleteAll();
      /*  Log.e("Insert: ", "Inserting ..");
        db.addContact(new ContactData("Ravi", "9100000000"));
        db.addContact(new ContactData("Srinivas", "9199999999"));
        db.addContact(new ContactData("Tommy", "9522222222"));
        db.addContact(new ContactData("Karthik", "9533333333"));
*/
//        contactData.clear();
//        contactData.addAll(db.getAllContacts());

        contactData = db.getAllContacts();
        sort();
        contactAdapter.addAll(contactData);
    }

    private void sort() {
        Collections.sort(contactData, new Comparator<ContactData>() {
            @Override
            public int compare(ContactData o1, ContactData o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    private void createContact(ContactData contact) {
        long id = db.addContact(contact);
        Utils.showToast(getApplicationContext(), "number Added Succesfully");
        Log.e("Contact", ">>" + contact.getId() + " " + contact.getName() + " " + contact.getNumber());
        ContactData n = db.getContact(id);
        contactData.add(n);
        int pos = contactAdapter.getItemCount();
        if (n != null) {
            contactAdapter.add(n, pos);
        }
    }

    private void updateContact(ContactData contact, int position) {
//        not updating in current basis implementation remaining
        ContactData n = contactData.get(position);
        n.setName(contact.getName());
        n.setNumber(contact.getNumber());
        db.updateContact(n);
        // refreshing the list
        contactData.set(position, n);
        sort();
//        contactAdapter.notifyItemChanged(position);
        contactAdapter.notifyDataSetChanged();
    }

    public void showDialog(View v, final int pos) {
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.view_add_contact, null);
        final PopupWindow popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
        edtNumber = view.findViewById(R.id.edtNumber);
        edtName = view.findViewById(R.id.edtName);
        btnAdd = view.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtName.getText().toString())) {
                    Utils.showToast(getApplicationContext(), "Enter Name");
                    return;
                } else if (TextUtils.isEmpty(edtNumber.getText().toString())) {
                    Utils.showToast(getApplicationContext(), "Enter Number");
                    return;
                }
                if (select == 1) {
                    updateContact(new ContactData(edtName.getText().toString(), edtNumber.getText().toString()), pos);
                } else if (select == 0) {
                    createContact(new ContactData(edtName.getText().toString(), edtNumber.getText().toString()));

                }
                popupWindow.dismiss();
            }
        });
    }
}
