package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.surajfirst.R;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.CommanConstants;
import com.example.surajfirst.Utils.CommenMethods;
import com.example.surajfirst.Utils.EndPointApi;
import com.example.surajfirst.Utils.NetworkCheck;
import com.example.surajfirst.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_verify, btn_resend;
    ImageView img_back;
    EditText edtV1, edtV2, edtV3, edtV4;
    public static String VERIFY_API_TAG = "verify";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        init();
    }


    private void init() {
        edtV1 = findViewById(R.id.edtV1);
        edtV2 = findViewById(R.id.edtV2);
        edtV3 = findViewById(R.id.edtV3);
        edtV4 = findViewById(R.id.edtV4);
        btn_verify = findViewById(R.id.btn_verify);
        img_back = findViewById(R.id.img_back);
        btn_resend = findViewById(R.id.btn_resend);

        btn_verify.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == btn_verify) {
            userVerify();

        } else if (v == img_back) {

            Intent intent = new Intent(VerificationActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (v == btn_resend) {

        }

    }

    private void userVerify() {
        if (NetworkCheck.isConnected(this)) {
            String var = edtV1.getText().toString() + edtV2.getText().toString() +
                    edtV3.getText().toString() + edtV4.getText().toString();

            final String currentTimeStemp = Utils.getCurrentTimeStemp();

            ApiConstants.ANDROID_DEVICE_ID = "1234";
            Map<String, String> param = new HashMap<>();
            param.put(ApiConstants.verification_code, var.trim());
            param.put(ApiConstants.USER_ID, Utils.getUserId(getApplicationContext()));

            String endPoint = EndPointApi.verify_verification_code + "?" + Utils.getStringForGetMethod(param);

            final JSONObject jString = new JSONObject(param);

            final String signature = Utils.encodeSha56(currentTimeStemp + CommanConstants.GET_Method + endPoint + "");

            final Map<String, String> headerMap = new HashMap<>();
            headerMap.put(ApiConstants.Authorization, Utils.getUserToken(getApplicationContext()));
            headerMap.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);
            headerMap.put(ApiConstants.API_TIMESTAMP, currentTimeStemp);
            headerMap.put(ApiConstants.API_SECRET, signature);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    ApiConstants.ROOT_URL2 + endPoint, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("verify response", " >> " + response.toString());
                    try {
                        String msg = response.getString("msg");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(VerificationActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headerMap;
                }
            };

            VolleySingleton volleySingleton = VolleySingleton.getInstance(this);
            volleySingleton.addToRequestQueue(jsonObjectRequest);

        } else {
            CommenMethods.showToast(this, getResources().getString(R.string.Internet_connectivity));
        }
    }
}
