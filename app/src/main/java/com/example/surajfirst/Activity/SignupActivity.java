package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.surajfirst.Interface.APIClient;
import com.example.surajfirst.Interface.APIInterface;
import com.example.surajfirst.R;
import com.example.surajfirst.Utils.ApiConstants;
import com.example.surajfirst.Utils.CommanConstants;
import com.example.surajfirst.Utils.CommenMethods;
import com.example.surajfirst.Utils.EndPointApi;
import com.example.surajfirst.Utils.NetworkCheck;
import com.example.surajfirst.Utils.Utils;
import com.example.surajfirst.view.RegularEditText;
import com.example.surajfirst.view.RegularTextView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout llSignup;
    private RegularTextView tv_login;
    private RegularEditText ev_signuemail, ev_signuppassword, ev_signupconfirmpass, ev_signupphone, ev_signupname;
    public static String SIGN_UP_TAG = "signup";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        init();
    }

    private void init() {
        tv_login = findViewById(R.id.tv_login);
        llSignup = findViewById(R.id.llSignup);

        ev_signuemail = findViewById(R.id.ev_signuemail);
        ev_signupconfirmpass = findViewById(R.id.ev_signupconfirmpass);
        ev_signupphone = findViewById(R.id.ev_signupphone);
        ev_signuppassword = findViewById(R.id.ev_signuppassword);
        ev_signupname = findViewById(R.id.ev_signupname);


        tv_login.setOnClickListener(this);
        llSignup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == tv_login) {
            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            startActivity(intent);
            //finish();
        }
        if (v == llSignup) {
            if (TextUtils.isEmpty(ev_signupname.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(ev_signuemail.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter email", Toast.LENGTH_SHORT).show();
            } else if (!ev_signuemail.getText().toString().trim().matches(CommanConstants.emailPattern)) {
                Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(ev_signupphone.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter phone number", Toast.LENGTH_SHORT).show();
            } else if (ev_signupphone.getText().toString().trim().length() != 10) {
                Toast.makeText(getApplicationContext(), "Invalid phone number", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(ev_signuppassword.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(ev_signupconfirmpass.getText().toString())) {
                Toast.makeText(getApplicationContext(), "enter password again", Toast.LENGTH_SHORT).show();
            } else if (!ev_signupconfirmpass.getText().toString().trim().matches(ev_signuppassword.getText().toString().trim())) {
                Toast.makeText(getApplicationContext(), "mismatch password", Toast.LENGTH_SHORT).show();
            } else {
//                userSignup();
                try {
                    signup();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // finish();
            }
        }
    }

    private void signup() throws IOException {
        if (NetworkCheck.isConnected(this)) {
            ev_signuemail.setText("robertdowney@abyssmail.com");
            ev_signupphone.setText("8058964777");
            ev_signuppassword.setText("System@123");
            ev_signupconfirmpass.setText("System@123");
            ev_signupname.setText("robert");

            final Map<String, String> stringMap = new HashMap<>();
            stringMap.put(ApiConstants.EMAIl, ev_signuemail.getText().toString().trim());
            stringMap.put(ApiConstants.PHONE_NUMBER, ev_signupphone.getText().toString().trim());
            stringMap.put(ApiConstants.PASSWORD, ev_signuppassword.getText().toString().trim());
            stringMap.put(ApiConstants.CONFIRM_PASSWORD, ev_signupconfirmpass.getText().toString().trim());
            stringMap.put(ApiConstants.NAME, ev_signupname.getText().toString().trim());
            stringMap.put(ApiConstants.USER_ID, ApiConstants.USER_ID_CUSTOMERS);

            final JSONObject jsonObject = new JSONObject(stringMap);


            APIInterface apiInterface = APIClient.getClient(getApplicationContext(), ApiConstants.POST, EndPointApi.singup, jsonObject.toString()).create(APIInterface.class);

            Call call = apiInterface.signup(jsonObject);

            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, retrofit2.Response response) {
                    Log.e("Signup", "> > " + new Gson().toJson(response.body()));
                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call call, Throwable t) {

                }
            });

        } else {
            CommenMethods.showToast(this, getResources().getString(R.string.Internet_connectivity));
        }

    }

    private void userSignup() {

        if (NetworkCheck.isConnected(this)) {
            ev_signuemail.setText("robertdowney@abyssmail.com");
            ev_signupphone.setText("8058964777");
            ev_signuppassword.setText("System@123");
            ev_signupconfirmpass.setText("System@123");
            ev_signupname.setText("robert");

            final String currentTimeStemp = Utils.getCurrentTimeStemp();

            ApiConstants.ANDROID_DEVICE_ID = "1234";
            ApiConstants.ANDROID_DEVICE_ID = "1234";

            final Map<String, String> stringMap = new HashMap<>();
            stringMap.put(ApiConstants.EMAIl, ev_signuemail.getText().toString().trim());
            stringMap.put(ApiConstants.PHONE_NUMBER, ev_signupphone.getText().toString().trim());
            stringMap.put(ApiConstants.PASSWORD, ev_signuppassword.getText().toString().trim());
            stringMap.put(ApiConstants.CONFIRM_PASSWORD, ev_signupconfirmpass.getText().toString().trim());
            stringMap.put(ApiConstants.NAME, ev_signupname.getText().toString().trim());
            stringMap.put(ApiConstants.USER_ID, ApiConstants.USER_ID_CUSTOMERS);

            final JSONObject jString = new JSONObject(stringMap);

            final String signature = Utils.encodeSha56(currentTimeStemp + CommanConstants.POST_Method + EndPointApi.singup + jString.toString());

            final Map<String, String> headerMap = new HashMap<>();
            headerMap.put(ApiConstants.Authorization, Utils.getUserToken(getApplicationContext()));
            headerMap.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);
            headerMap.put(ApiConstants.API_TIMESTAMP, currentTimeStemp);
            headerMap.put(ApiConstants.API_SECRET, signature);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    ApiConstants.URL_SIGNUP, jString, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("signup response", " >> " + response.toString());
                    try {
                        String msg = response.getString("msg");
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(new Intent(SignupActivity.this, VerificationActivity.class));

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headerMap;
                }
            };

            VolleySingleton volleySingleton = VolleySingleton.getInstance(this);
            volleySingleton.addToRequestQueue(jsonObjectRequest);

        } else {
            CommenMethods.showToast(this, getResources().getString(R.string.Internet_connectivity));
        }


    }


}
