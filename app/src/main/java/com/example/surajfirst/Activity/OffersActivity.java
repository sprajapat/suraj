package com.example.surajfirst.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.Adapter.OffersAdapter;
import com.example.surajfirst.R;


public class OffersActivity extends AppCompatActivity {
    //    private List<OffersAdapter> data = new ArrayList<>();
    private OffersAdapter offersAdapter;
    private RecyclerView recyclerView;
    int images[] = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img2, R.drawable.img3, R.drawable.img1, R.drawable.img3, R.drawable.img2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        init();
    }

    private void init() {
        recyclerView = findViewById(R.id.rcylrOffers);

        offersAdapter = new OffersAdapter(this, images);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerdashB.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(offersAdapter);

    }
}
