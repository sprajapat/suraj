package com.example.surajfirst.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.Adapter.DashBAdapter;
import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.DashBData;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.ArrayList;
import java.util.List;

public class DashBoardActivity extends AppCompatActivity {

    private List<DashBData> dashBdata = new ArrayList<>();
    private DashBAdapter dashBAdapter;
    private RecyclerView recyclerdashB;
    private Fragment currentFragment;
    LinearLayout llContainer;
    public SlidingRootNav slidingRootNav;
    ImageView img_ellipsis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);
        init(savedInstanceState);
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withDragDistance(150) //Horizontal translation of a view. Default == 180dp
                .withRootViewScale(0.7f) //Content view's scale will be interpolated between 1f and 0.7f. Default == 0.65f;
                .withRootViewElevation(10) //Content view's elevation will be interpolated between 0 and 10dp. Default == 8.
                .withRootViewYTranslation(3)
                .withMenuLayout(R.layout.drawer_nav_menu)
                .inject();

    }

    private void init(Bundle savedInstanceState) {
        llContainer = findViewById(R.id.llContainer);
        img_ellipsis = findViewById(R.id.img_ellipsis);
        img_ellipsis = findViewById(R.id.img_ellipsis);
        img_ellipsis = findViewById(R.id.img_ellipsis);


        recyclerdashB = findViewById(R.id.recyclerdashB);
        dashBAdapter = new DashBAdapter(this);
        recyclerdashB.setLayoutManager(new LinearLayoutManager(DashBoardActivity.this));
//        recyclerdashB.setItemAnimator(new DefaultItemAnimator());
        recyclerdashB.setAdapter(dashBAdapter);

        img_ellipsis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingRootNav.openMenu();
            }
        });

        dashBAdapter.setEventListener(new DashBAdapter.OnItemclickListener() {
            @Override
            public void onShortClick(View view, int position) {
                dashBAdapter.changeSelection(position);
            }
        });
        prepareData();
    }

    private void prepareData() {
        dashBdata.clear();
        dashBdata.add(new DashBData(R.mipmap.profile, "MyProfile"));
        dashBdata.add(new DashBData(R.mipmap.offer, "Offers"));
        dashBdata.add(new DashBData(R.mipmap.my_order, "My Orders"));
        dashBdata.add(new DashBData(R.mipmap.notification, "Notifications"));
        dashBdata.add(new DashBData(R.mipmap.support, "Support"));
        dashBdata.add(new DashBData(R.mipmap.rate, "Rate Us"));
        dashBdata.add(new DashBData(R.mipmap.setting, "Settings"));

        dashBAdapter.addAll(dashBdata);
    }

    public void changeFragmentdashboard(Fragment targetFragment) {
        currentFragment = targetFragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.llContainer, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


}
