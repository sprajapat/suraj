package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surajfirst.R;
import com.example.surajfirst.view.RegularTextView;

public class ForgotPassActivity extends AppCompatActivity implements View.OnClickListener {
    private RegularTextView tv_backtologin;
    ImageView btn_back;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);

        init();
    }

    public void init() {
        tv_backtologin = findViewById(R.id.tv_backtologin);
        btn_back = findViewById(R.id.btn_back);
        btn_submit = findViewById(R.id.btn_submit);
        btn_back.setOnClickListener(this);
        tv_backtologin.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v==tv_backtologin ){
            Intent intent = new Intent(ForgotPassActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
        if (v==btn_back) {
            Intent intent = new Intent(ForgotPassActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();

        }
        if (v==btn_submit) {

        }
    }
}
