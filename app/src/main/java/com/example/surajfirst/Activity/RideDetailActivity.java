package com.example.surajfirst.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surajfirst.R;
import com.example.surajfirst.Utils.CommanConstants;

public class RideDetailActivity extends AppCompatActivity {
    TextView txtTime, txtPerson, txtDate, txtStatus, txtPayType, txtAmount;
    ImageView imgPerson;
    LinearLayout llLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);
        Intent intent = getIntent();

        init();

    }

    private void init() {
        imgPerson = findViewById(R.id.imgPerson);
        txtPerson = findViewById(R.id.txtPerson);
        txtStatus = findViewById(R.id.txtStatus);
        txtAmount = findViewById(R.id.txtAmount);
        txtPayType = findViewById(R.id.txtPayType);
        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        llLogin = findViewById(R.id.llLogin);

        txtPerson.setText(getIntent().getStringExtra(CommanConstants.PERSON));
        txtStatus.setText(getIntent().getStringExtra(CommanConstants.STATUS));
        txtAmount.setText(getIntent().getStringExtra(CommanConstants.AMOUNT));
        txtPayType.setText(getIntent().getStringExtra(CommanConstants.PAYTYPE));
        txtDate.setText(getIntent().getStringExtra(CommanConstants.DATE));
        txtTime.setText(getIntent().getStringExtra(CommanConstants.TIME));
        Bundle bundle = this.getIntent().getExtras();
        imgPerson.setImageResource(bundle.getInt(CommanConstants.IMAGEID));
//        imgPerson.setImageResource(getIntent().getIntExtra(CommanConstants.IMAGEID));
        llLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FacebookLogin.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  Intent intent = new Intent(this, MyOrdersActivity.class);
        startActivity(intent);
        finish();*/
    }
}
