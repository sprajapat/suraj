package com.example.surajfirst.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surajfirst.R;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgWallet, imgNetBanking, imgHDFC, imgDebit;
    LinearLayout llNetBanking, llDebit, llHDFC, llWallet, llCredit, llDebitEx;
    View viewDebit;
    Button btnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment);
        init();
    }

    private void init() {
        imgWallet = findViewById(R.id.imgWallet);
        imgDebit = findViewById(R.id.imgDebit);
        imgHDFC = findViewById(R.id.imgHDFC);
        imgNetBanking = findViewById(R.id.imgNetBanking);
        viewDebit = findViewById(R.id.viewDebit);
        btnPay = findViewById(R.id.btnPay);

        llNetBanking = findViewById(R.id.llNetBanking);
        llHDFC = findViewById(R.id.llHDFC);
        llWallet = findViewById(R.id.llWallet);
        llDebit = findViewById(R.id.llDebit);
        llCredit = findViewById(R.id.llCredit);
        llDebitEx = findViewById(R.id.llDebitEx);

        imgWallet.setImageResource(R.mipmap.radio_checked_b);
        viewDebit.setVisibility(View.VISIBLE);
        llCredit.setVisibility(View.VISIBLE);
        llHDFC.setVisibility(View.GONE);
        llDebitEx.setVisibility(View.GONE);

        llWallet.setOnClickListener(this);
        llCredit.setOnClickListener(this);
        llDebit.setOnClickListener(this);
        llNetBanking.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        imgWallet.setImageResource(R.mipmap.radio_blank);
        imgHDFC.setImageResource(R.mipmap.radio_blank);
        imgDebit.setImageResource(R.mipmap.radio_blank);
        imgNetBanking.setImageResource(R.mipmap.radio_blank);
        llCredit.setVisibility(View.VISIBLE);
        llHDFC.setVisibility(View.GONE);
        viewDebit.setVisibility(View.VISIBLE);

        switch (v.getId()) {
            case R.id.llWallet:
                imgWallet.setImageResource(R.mipmap.radio_checked_b);
                llDebitEx.setVisibility(View.GONE);
                break;
            case R.id.llDebit:
                imgDebit.setImageResource(R.mipmap.radio_checked_b);
                viewDebit.setVisibility(View.GONE);
                llDebitEx.setVisibility(View.VISIBLE);
                break;
            case R.id.llCredit:
                imgHDFC.setImageResource(R.mipmap.radio_checked_b);
                llHDFC.setVisibility(View.VISIBLE);
                llCredit.setVisibility(View.GONE);
                llDebitEx.setVisibility(View.GONE);
                break;
            case R.id.llNetBanking:
                imgNetBanking.setImageResource(R.mipmap.radio_checked_b);
                break;
            case R.id.btnPay:
                break;

        }
    }
}
