package com.example.surajfirst.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.surajfirst.Adapter.OrdersAdapter;
import com.example.surajfirst.R;
import com.example.surajfirst.ModalRes.YourRideData;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class MyOrdersActivity extends AppCompatActivity {
    private List<YourRideData> yourRideData = new ArrayList<>();
    public OrdersAdapter ordersAdapter;
    RecyclerView recyclerView;
    public TextView txtNumber, txtTop;
    ImageView imgEllisis, imgBack, imgDelet, imgHelp, imgEllisis2;
    int select = -1;
    LinearLayout llCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        init();
    }

    private void init() {
        recyclerView = findViewById(R.id.rcylrOrder);
        imgEllisis = findViewById(R.id.imgEllisis);
        imgBack = findViewById(R.id.imgBack);
        txtNumber = findViewById(R.id.txtNumber);
        imgDelet = findViewById(R.id.imgDelet);
        imgHelp = findViewById(R.id.imgHelp);
        txtTop = findViewById(R.id.txtTop);
        imgEllisis2 = findViewById(R.id.imgEllisis2);
        llCoordinator = findViewById(R.id.llCoordinator);


        ordersAdapter = new OrdersAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ordersAdapter);

        ordersAdapter.setEventListener(new OrdersAdapter.OnItemClickListener() {

            @Override
            public void onShortClick(View view, final int position) {
                YourRideData itemData = ordersAdapter.getItemData(position);

                select = ordersAdapter.changeSelection(position);

                if (select == 0) {
                    setToolBarIni();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
//                YourRideData itemData = ordersAdapter.getItemData(position);
//                ordersAdapter.changeSelection(position);
                select = ordersAdapter.changeLongSelection(position);

                imgEllisis.setVisibility(View.GONE);
                imgBack.setVisibility(View.VISIBLE);
                txtNumber.setVisibility(View.VISIBLE);
                imgDelet.setVisibility(View.VISIBLE);
                imgHelp.setVisibility(View.VISIBLE);
                txtTop.setVisibility(View.GONE);
                imgEllisis2.setVisibility(View.VISIBLE);

                if (select == 0) {
                    setToolBarIni();
                }
            }
/*
            @Override
            public void onMoved(RecyclerView.ViewHolder view, int position) {
                Toast.makeText(getApplicationContext(), "on Move", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder view, int position) {
                Toast.makeText(getApplicationContext(), "on Swiped ", Toast.LENGTH_SHORT).show();


            }*/


        });

        imgDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select = ordersAdapter.deletMulti();
                if (select == 0) {
                    setToolBarIni();
                }
            }
        });


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();
                final YourRideData item = ordersAdapter.getItemData(position);

                ordersAdapter.removeItem(position);


                Snackbar snackbar = Snackbar.make(llCoordinator, "Item was removed from the list.", Snackbar.LENGTH_LONG)
                        .setAction("UNDO", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ordersAdapter.restoreItem(item, position);
                                recyclerView.scrollToPosition(position);
                            }
                        });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        prepareData();
    }

    public void setToolBarIni() {
        imgEllisis.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.GONE);
        txtNumber.setVisibility(View.GONE);
        imgDelet.setVisibility(View.GONE);
        imgHelp.setVisibility(View.GONE);
        txtTop.setVisibility(View.VISIBLE);
        imgEllisis2.setVisibility(View.GONE);
    }


    private void prepareData() {
        yourRideData.clear();
        yourRideData.add(new YourRideData("Ronak", "Canceled", "50.0", "Cash", "12/01/2020, ", "5.00 PM", R.drawable.img_ronak));
        yourRideData.add(new YourRideData("Raj", "Success", "40.0", "Cash", "12/01/2020, ", "4.00 PM", R.drawable.img_raj));
        yourRideData.add(new YourRideData("Mukesh", "Scheduled", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Piyush", "Scheduled", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Jitendra", "ffff", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Kamal", "Scheduled", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Mukddesh", "Scheduled", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Mukesh", "ssff", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("dd", "sdff", "50.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Mukesh", "Canceled", "30.0", "Cash", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Ram", "Canceled", "10.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Rakesh", "Scheduled", "22.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Mukesh", "Scheduled", "50.0", "Cash", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Rohit", "Success", "54.0", "Wallet", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));
        yourRideData.add(new YourRideData("Pankaj", "Scheduled", "55.0", "Cash", "12/01/2020, ", "3.00 PM", R.drawable.img_mukesh));

        ordersAdapter.addAll(yourRideData);
    }

    @Override
    public void onBackPressed() {
        MyOrdersActivity.super.onBackPressed();
    }
}

